<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoNormativa extends Model
{
    protected $table = 'tipo_normativa';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'nombre'
    ];
    
    /**
     * Definicion de relacion de extension.
     */
    public function normativas() 
    {
        return $this->hasMany(Normativa::class);
    }    
}