<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parcela extends Model
{
    use SoftDeletes;

    public $fillable = ['edad_parcela','fecha_parcela', 'comunidad_id', 'latitud', 'longitud', 'descripcion', 'hectareas', 'motivo_actualizacion_id', 'region_id', 'user_id','autorizacion_id', 'zona_racionalizada_id', 'codigo_catastral'];


    public function scopeSearch($query, $row, $arg){
        if (!$row) {
            return $query;
        }
        return $query->whereRaw("$row like '%$arg%'");
    }
    public function personas(){
        return $this->belongsToMany(Persona::class, 'persona_parcela');
    }

    public function comunidad(){
        return $this->belongsTo(Comunidad::class);
    }

    public function motivo_actualizacion(){
        return $this->belongsTo(MotivoActualizacion::class);
    }

    public function region(){
        return $this->belongsTo(Regional::class);
    }

    public function personaparcela()
    {
    return $this->belongsTo(PersonaParcela::class);
    }

    public function autorizacionrenovacion()
    {
        return $this->belongsTo(AutorizacionRenovacion::class);
    }
 
    public function zona_racionalizada(){
        return $this->belongsTo(zona_racionalizada::class);
    }
}
