<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ControlPuesto extends Model
{
    protected $table = 'control_puesto';
    protected $fillable = ['control_id', 'puesto_id', 'user_id'];

    public function control_hoja_ruta(){
        return $this->belongsTo(ControlHojaRuta::class);
    }

    public function puesto_control(){
        return $this->belongsTo(PuestoControl::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
    static function verificaPuestoControl($hoja_ruta_id) {
        $puestos_ruta = DB::table('hoja_rutas')
                    ->join('rutas', 'rutas.id','=', 'hoja_rutas.ruta_id')
                    ->join('ruta_puesto', 'ruta_puesto.ruta_id', '=', 'rutas.id')
                    ->where('hoja_rutas.id', '=', $hoja_ruta_id)
                    ->select('ruta_puesto.*')
                    ->orderBy('ruta_puesto.id')
                    ->get();
        $puesto_controlados = DB::table('control_hoja_rutas as chr')
                                ->join('control_puesto as cp', 'chr.id', '=', 'cp.control_id')
                                ->where('chr.hoja_ruta_id', '=', $hoja_ruta_id)
                                ->select('cp.*')
                                ->orderBy('cp.id')
                                ->get();
        $comparador = true;
        for ($i=0; $i < count($puesto_controlados); $i++) {
            if($puestos_ruta[$i]->puesto_control_id != $puesto_controlados[$i]->puesto_id){
                $comparador = false;
                break;
            }
        }
        return $comparador;
    }
}
