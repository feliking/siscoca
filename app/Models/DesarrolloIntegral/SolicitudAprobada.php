<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SolicitudAprobada extends Model
{
    use SoftDeletes;

    protected $table = 'solicitud_aprobadas';
    public $timestamps = true;
    public $guarded = ['id'];

    protected $fillable = ['solicitud_id', 'proyecto_id'];

    public function solicitud() {
        return $this->belongsTo(Solicitud::class);
    }
    public function proyecto() {
        return $this->belongsTo(Proyecto::class);
    }
    public function ordenProcederProyecto(){
        return $this->hasOne(OrdenProcederProyecto::class);
    }
    public function reprogramacion() {
        return $this->hasMany(reprogramacionProyecto::class);
    }
}
