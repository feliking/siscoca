<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Convocatoria extends Model
{
    use SoftDeletes;
    protected $table = 'convocatorias';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_convocatoria_id', 'fecha_inicio','fecha_fin','terminos_referencia','indicaciones_calificables',
        'codigo_referencia','tipo_proyecto', 'apertura', 'calificacion'
    ];

    public function solicitudes() 
    {
        return $this->hasMany(Solicitud::class);
    }  
    
    public function tipo_convocatoria() 
    {
        return $this->belongsTo(TipoConvocatoria::class);
    } 

    public function propuestas()
    {
        return $this->hasMany(Propuesta::class);
    }

    public function requisitos(){
        return $this->hasMany(Requisito::class);
    }
    public function proyecto() 
    {
        return $this->belongsTo(Proyecto::class);
    } 

    
}
