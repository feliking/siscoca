<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;

class seguimientoProyecto extends Model
{
    protected $table = 'seguimiento_proyectos';
    public $timestamps = true;
    public $guarded = ['id'];

    protected $fillable = ['proyecto_id','fecha_visita','porcentaje_avance', 'observaciones', 'adjuntos'];

    public function proyecto(){
        return $this->belongsTo(Proyecto::class);
    }
    
}
