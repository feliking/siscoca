<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EjecucionProyecto extends Model
{
    use SoftDeletes;
    public $fillable = ['fecha', 'responsable', 'contrato', 'actividades', 'cronograma', 'proyecto_id'];

    public function proyecto(){
        return $this->belongsTo(Proyecto::class);
    }
}
