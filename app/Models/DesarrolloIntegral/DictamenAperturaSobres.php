<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DictamenAperturaSobres extends Model
{
    use SoftDeletes;
    protected $fillable = ['fecha', 'informe', 'adjuntos', 'observaciones', 'solicitud_id'];

    public function convocatoria(){
        return $this->belongsTo(Convocatoria::class);
    }
}
