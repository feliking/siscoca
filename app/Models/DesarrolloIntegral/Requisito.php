<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;

class Requisito extends Model
{
    protected $fillable = ['nombre', 'convocatoria_id'];
}
