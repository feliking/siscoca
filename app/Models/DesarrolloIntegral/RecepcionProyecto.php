<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;

class RecepcionProyecto extends Model
{
    protected $table = 'recepcion_proyectos';
    public $timestamps = true;
    public $guarded = ['id'];
    protected $fillable = ['proyecto_id','solicitud_aprobada_id', 'fecha_recepcion','observaciones', 'lista_adjuntos'];

    public function proyecto() {
        return $this->belongsTo(Proyecto::class);
    }
    public function solicitudaprobada(){
        return $this->belongsTo(SolicitudAprobada::class);
    }
}
