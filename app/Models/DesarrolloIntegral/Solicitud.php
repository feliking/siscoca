<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Solicitud extends Model
{
    use  SoftDeletes;
    protected $table = 'solicitudes';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'razon_social', 'fecha_recepcion','convocatoria_id','codigo_solicitud', 'apertura', 'calificacion'
    ];    

    public function convocatoria(){
        return $this->belongsTo(Convocatoria::class);
    }

    public function propuestas(){
        return $this->hasMany(Propuesta::class);
    }

    public function dictamen_apertura_sobres(){
        return $this->hasMany(DictamenAperturaSobres::class);
    }
}
