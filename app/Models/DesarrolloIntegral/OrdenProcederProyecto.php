<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;

class OrdenProcederProyecto extends Model
{
    protected $table = 'orden_proceder_proyectos';
    public $timestamps = true;
    public $guarded = ['id'];

    protected $fillable = ['fecha_emision', 'responsable_nombre', 'observaciones',
                            'solicitud_aprobada_id'];
    
    public function solicitud(){
        return $this->belongsTo(Solicitud::class);
    }
}
