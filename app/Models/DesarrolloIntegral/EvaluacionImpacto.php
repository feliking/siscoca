<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EvaluacionImpacto extends Model
{
    use SoftDeletes;
    protected $table = 'evaluacion_impactos_oii';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'proyecto_id', 'fecha','responsable','fecha_cierre','presupuesto_ejecutado',
        'fotografias','conclusiones'
    ];

    public function proyecto() 
    {
        return $this->belongsTo(Proyecto::class);
    } 
}
