<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;

class evaluacion_impacto extends Model {
    protected $table = 'evaluacion_impacto';
    public $timestamps = true;
    public $guarded = ['id'];

    public function capacitacion() {
        return $this->belongsTo(Capacitacion::class);
    }
}
