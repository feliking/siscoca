<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;

class Propuesta extends Model
{
    protected $fillable=['nombre', 'nombre_representante', 'solicitud_id', 'aceptado'];

    public function solicitud(){
        return $this->belongsTo(Solicitud::class);
    }
}
