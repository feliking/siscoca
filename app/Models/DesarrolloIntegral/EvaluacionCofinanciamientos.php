<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EvaluacionCofinanciamientos extends Model
{
    use SoftDeletes;
    public $fillable = ['proyecto_id', 'indicadores', 'impacto_proyecto', 'sostenibilidad', 'presupuesto', 'coste_eficacia', 'monto'];

    public function proyecto(){
        return $this->belongsTo(Proyecto::class);
    }
}
