<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EvaluacionViabilidad extends Model
{
    use SoftDeletes;
    protected $table='evaluacion_viabilidads';
    public $timestamps = true;
    public $guarded =['id'];

    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = 
    [
        'proyecto_id','responsable','fecha','estado','informe_adjunto','presupuesto'        
    ];
    
    public function proyecto() 
    {
        return $this->belongsTo(Proyecto::class);
    }  

}
