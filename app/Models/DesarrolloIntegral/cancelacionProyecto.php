<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use illuminate\Database\Eloquent\SoftDeletes;

class cancelacionProyecto extends Model
{
    use SoftDeletes;
    protected $table = 'cancelacion_proyectos';
    public $timestamps = true;
    public $guarded = ['id'];

    protected $fillable = ['proyecto_id','solicitud_aprobada_id', 'observaciones', 'fecha_cancelacion', 'responsable'];

    public function proyecto() {
        return $this->belongsTo(Proyecto::class);
    }
}
