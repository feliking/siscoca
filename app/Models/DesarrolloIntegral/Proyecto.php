<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proyecto extends Model
{
    use SoftDeletes;
    protected $table='proyectos';
    public $timestamps = true;
    public $guarded =['id'];

    protected $fillable = ['nombre', 'municipio_id','convocatoria_id'];

    public function municipio(){
        return $this->belongsTo(\App\Municipio::class);
    }
    public function cancelacion(){
        return $this->hasOne(cancelacionProyecto::class);
    }
    public function solicitudAprobada(){
        return $this->hasMany(SolicitudAprobada::class);
    }
    public function seguimientoproyecto(){
        return $this->hasMany(seguimientoProyecto::class);
    }
    public function reprogramacionproyecto(){
        return $this->hasMany(reprogramacionProyecto::class);
    }
    public function recepcionProyecto(){
        return $this->hasOne(RecepcionProyecto::class);
    }
    public function evaluacion_cofinanciamientos(){
        return $this->hasMany(EvaluacionCofinanciamientos::class);
    }
    public function ejecucion_proyecto(){
        return $this->hasMany(EjecucionProyecto::class);
    }  

    public function convocatoria(){
        return $this->belongsTo(Convocatoria::class);
    }
    public function priorizacions()
    {
        return $this->hasMany(Priorizacion::class);
    }
    public function evaluacionviabilidads()
    {
        return $this->hasMany(EvaluacionViabilidad::class);
    }
    public function supervisions()
    {
        return $this->hasMany(Supervision::class);
    }
    public function cierre_obras()
    {
        return $this->hasMany(CierreObra::class);
    }
    public function evaluacion_impactos()
    {
        return $this->hasMany(EvaluacionImpacto::class);
    }  
}
