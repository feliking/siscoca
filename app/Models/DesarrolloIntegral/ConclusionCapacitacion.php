<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;

class ConclusionCapacitacion extends Model {
    protected $table = 'conclusiones';
    public $timestamps = true;
    public $guarded = ['id'];

    protected $fillable = [
        'id', 'fecha_conclusion', 'presupuesto_ejecutado', 'informe', 'observaciones',
        'capacitacion_id'
    ];
    public function capacitacion() {
        return $this->belongsTo(Capacitacion::class);
    }
}
