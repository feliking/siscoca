<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;

class reprogramacionProyecto extends Model
{
    protected $table = 'reprogramacion_proyectos';
    public $timestamps = true;
    public $guarded = ['id'];

    protected $fillable = ['proyecto_id', 'solicitud_aprobada_id', 'fecha_reprogramacion','responsable', 'observaciones'];

    public function proyecto(){
        return $this->belongsTo(Proyecto::class);
    } 
}
