<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Capacitacion extends Model {
    use SoftDeletes;
    protected $table = 'capacitaciones';
    public $timestamps = true;
    public $guarded = ['id'];

    protected $fillable = [
        'id', 'fecha_capacitacion', 'tipo_actividad', 'organizacion', 'responsable_nombre',
        'presupuesto', 'proyecto_id'
    ];

    public function scopeSearch($query, $search)
    {
        if (!$search) {
            return $query;
        }
        return $query->whereRaw('searchtext @@ plainto_tsquery(\'spanish\', ?)', [$search])
            ->orderByRaw('ts_rank(searchtext, plainto_tsquery(\'spanish\', ?)) DESC', [$search]);
    }
    
    public function conclusionCapacitacion() {
        return $this->hasOne(ConclusionCapacitacion::class);
    }
    public function evaluacion_impacto() {
        return $this->hasOne(evaluacion_impacto::class);
    }
}
