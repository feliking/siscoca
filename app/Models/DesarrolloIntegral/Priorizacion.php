<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Priorizacion extends Model
{
    use SoftDeletes;
    protected $table = 'priorizacions';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'proyecto_id', 'nombre_propuesta','fecha_creacion','nombre_representante',
        'priorizacion','conclusiones'
    ]; 
          
    public function proyecto() 
    {
        return $this->belongsTo(Proyecto::class);
    } 
}
