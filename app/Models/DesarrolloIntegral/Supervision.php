<?php

namespace App\Models\DesarrolloIntegral;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supervision extends Model
{
    use SoftDeletes;
    protected $table = 'supervisions';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'proyecto_id','fecha','responsable', 'fecha_visita','porcentaje_avance','fotografias' 
    ];
    
    public function proyecto() 
    {
        return $this->hasMany(Proyecto::class);
    }  
}