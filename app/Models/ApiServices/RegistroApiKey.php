<?php

namespace App\Models\ApiServices;

use Illuminate\Database\Eloquent\Model;
use Illuminate\DataBase\Eloquent\SoftDeletes;

class RegistroApiKey extends Model
{
    use SoftDeletes;
    protected $table = 'registro_api_keys';
    public $timestamps = true;
    public $guarded = ['id'];

    protected $fillable = [
        'apiKey', 'fecha_finalizacion', 'entidad_id'
    ];
}
