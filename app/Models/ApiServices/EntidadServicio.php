<?php

namespace App\Models\ApiServices;

use Illuminate\Database\Eloquent\Model;
use Illuminate\DataBase\Eloquent\SoftDeletes;

class EntidadServicio extends Model
{
    use SoftDeletes;
    protected $table = 'entidad_servicios';
    public $timestamps = true;
    public $guarded = ['id'];

    protected $fillable = [
        'nombre', 'responsable', 'descripcion', 'user_id', 'apiKey'
    ];

    public function usuario (){
        return $this->belongsTo(\App\User::class);
    }
}
