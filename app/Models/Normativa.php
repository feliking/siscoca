<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Normativa extends Model
{
    use SoftDeletes;
    protected $table = 'normativa';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_normativa_id', 'nombre', 'descripcion', 'adjunto'
    ];
    
    /**
     * Definicion de relaciones.
     */
    public function tipo_normativa() 
    {
        return $this->belongsTo(TipoNormativa::class);
    }

}