<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class unidad_dependiente extends Model
{
    protected $table = 'unidad_dependientes';
    public $timestamps = true;
    public $guarded = ['id'];

    protected $fillable = [
        'descripcion'
    ];
    //
    public function unidad () {
        return $this->belongsTo(tipo_unidad::class);
    }
    
}
