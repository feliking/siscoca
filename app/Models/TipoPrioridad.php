<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPrioridad extends Model
{
    protected $table = 'tipos_prioridad';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'nombre'
    ];
    
    /**
     * Definicion de relacion de extension.
     */
    public function eventos() 
    {
        return $this->hasMany(Evento::class);
    }  
}
