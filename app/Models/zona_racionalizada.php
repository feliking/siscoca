<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class zona_racionalizada extends Model
{
    public $fillable = ['fecha_racionalizacion', 'departamento_id', 'provincia_id', 'regional_id', 'parcela_id'];

    public function parcela() {
        return $this->hasOne(Parcela::class);
    }
    public function tipoEjecucion(){
        return $this->hasOne(tipoEjecucion::class);
    }
}
