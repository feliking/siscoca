<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_unidad extends Model
{
    protected $table = 'tipo_unidades';
    public $timestamps = true;
    public $guarded = ['id'];
    //
    protected $fillable = [
        'descripcion'
    ];

    public function unidades(){
        return $this->hasMany(unidad_dependiente::class);
    }
}
