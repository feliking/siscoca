<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Reportes extends Model
{
    public  static function hojaRuta($fecha_ini, $fecha_fin)
    {
        return DB::table('hoja_rutas')
            ->leftJoin('personas', 'personas.id', '=', 'hoja_rutas.persona_id')
            ->leftJoin('comercializador_infraccions', 'personas.id', '=', 'comercializador_infraccions.persona_id')
            ->leftJoin('sanciones', 'sanciones.id', '=', 'comercializador_infraccions.sancion_id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'personas.departamento_extension_id')
            ->select('personas.nombre', 'personas.primer_apellido', 'personas.segundo_apellido', 'personas.carnet_identidad', 'departamentos.sigla', 'personas.telefono', 'sanciones.codigo', 'sanciones.monto', 'hoja_rutas.taques', 'hoja_rutas.codigo_comercializador', 'hoja_rutas.correlativo', DB::raw("(CASE WHEN (hoja_rutas.estado = false) THEN 'En Proceso' ELSE 'Finalizado' END) as estado"), DB::raw("to_char(hoja_rutas.created_at, 'DD-MON-YYYY') as creacion"))
            ->whereBetween(DB::raw('DATE(hoja_rutas.updated_at)'), [$fecha_ini, $fecha_fin])
            ->get();
    }
    public static function headingHojaRuta()
    {
        return ['Nombres', 'Apellido Paterno', 'Apellido Materno', 'CI', 'Expedido', 'Teléfono/Celular', 'Código de sanción', 'Monto sanción', 'Nro Taques', 'Código comercializador', 'Correlativo', 'Estado', 'Fecha de creación'];
    }
    public static function cantidadReport()
    {
        $prod = Persona::where('tipo_persona_id', 1)->count();
        $prod_detalle = Persona::where('tipo_persona_id', 2)->count();
        $comer = Persona::where('tipo_persona_id', 3)->count();
        $query =
            [
                'cantidad_productores' => strval($prod),
                'cantidad_productores2' =>  strval($prod_detalle),
                'cantidad_comercializadores' =>  strval($comer),
            ];
        return $query;
    }
    public static function heading_cantidadReport()
    {
        return ['Productores', 'Productores al detalle', 'Comercializadores'];
    }
    public static function infoGralProdReport()
    {
        return DB::table("personas")
            ->leftJoin('comercializador_infraccions', 'personas.id', '=', 'comercializador_infraccions.persona_id')
            ->leftJoin('sanciones', 'sanciones.id', '=', 'comercializador_infraccions.sancion_id')
            ->leftJoin('infracciones', 'infracciones.id', '=', 'sanciones.infraccion_id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'personas.departamento_extension_id')
            ->leftJoin('resoluciones_administrativas', 'resoluciones_administrativas.persona_id', '=', 'personas.id')
            ->leftJoin('tipos_resolucion', 'tipos_resolucion.id', '=', 'resoluciones_administrativas.tipo_resolucion_id')
            ->select('personas.nombre', 'personas.primer_apellido', 'personas.segundo_apellido', 'personas.carnet_identidad', 'departamentos.sigla', 'personas.telefono', 'sanciones.codigo as code_san', 'sanciones.monto', 'resoluciones_administrativas.resolucion',  'tipos_resolucion.nombre as tipo')
            ->where('personas.tipo_persona_id', 1)
            ->get();
    }
    public static function heading_infoGralProdReport()
    {
        return ['Nombre', 'Primer apellido', 'Segundo Apellido', 'CI', 'Expedido', 'Telefono', 'Código sanción', 'Monto', 'Resolución', 'Tipo'];
    }
    public static function infoGralProdDetReport()
    {
        return DB::table("personas")
            ->leftJoin('comercializador_infraccions', 'personas.id', '=', 'comercializador_infraccions.persona_id')
            ->leftJoin('sanciones', 'sanciones.id', '=', 'comercializador_infraccions.sancion_id')
            ->leftJoin('infracciones', 'infracciones.id', '=', 'sanciones.infraccion_id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'personas.departamento_extension_id')
            ->leftJoin('resoluciones_administrativas', 'resoluciones_administrativas.persona_id', '=', 'personas.id')
            ->leftJoin('tipos_resolucion', 'tipos_resolucion.id', '=', 'resoluciones_administrativas.tipo_resolucion_id')
            ->select('personas.nombre', 'personas.primer_apellido', 'personas.segundo_apellido', 'personas.carnet_identidad', 'departamentos.sigla', 'personas.telefono', 'sanciones.codigo as code_san', 'sanciones.monto', 'resoluciones_administrativas.resolucion',  'tipos_resolucion.nombre as tipo')
            ->where('personas.tipo_persona_id', 2)
            ->get();
    }
    public static function heading_infoGralProdDetReport()
    {
        return ['Nombre', 'Primer apellido', 'Segundo Apellido', 'CI', 'Expedido', 'Telefono', 'Código sanción', 'Monto', 'Resolución', 'Tipo'];
    }
    public static function infoGralComReport()
    {
        return DB::table("personas")
            ->leftJoin('comercializador_infraccions', 'personas.id', '=', 'comercializador_infraccions.persona_id')
            ->leftJoin('sanciones', 'sanciones.id', '=', 'comercializador_infraccions.sancion_id')
            ->leftJoin('infracciones', 'infracciones.id', '=', 'sanciones.infraccion_id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'personas.departamento_extension_id')
            ->leftJoin('resoluciones_administrativas', 'resoluciones_administrativas.persona_id', '=', 'personas.id')
            ->leftJoin('tipos_resolucion', 'tipos_resolucion.id', '=', 'resoluciones_administrativas.tipo_resolucion_id')
            ->select('personas.nombre', 'personas.primer_apellido', 'personas.segundo_apellido', 'personas.carnet_identidad', 'departamentos.sigla', 'personas.telefono', 'sanciones.codigo as code_san', 'sanciones.monto', 'resoluciones_administrativas.resolucion',  'tipos_resolucion.nombre as tipo')
            ->where('personas.tipo_persona_id', 3)
            ->get();
    }
    public static function heading_infoGralComReport()
    {
        return ['Nombre', 'Primer apellido', 'Segundo Apellido', 'CI', 'Expedido', 'Telefono', 'Código sanción', 'Monto', 'Resolución', 'Tipo'];
    }
    public static function infoGralEmpReport()
    {
        return DB::table("empresas")
            ->leftJoin('personas', 'personas.id', '=', 'empresas.persona_id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'personas.departamento_extension_id')
            ->select('personas.nombre', 'personas.primer_apellido', 'personas.segundo_apellido', 'personas.carnet_identidad', 'departamentos.sigla', 'personas.telefono', 'empresas.nombre as nomb_empre', 'empresas.nit', 'empresas.numero_fundempresa')
            ->get();
    }
    public static function heading_infoGralEmpReport()
    {
        return ['Nombre', 'Primer apellido', 'Segundo Apellido', 'CI', 'Expedido', 'Telefono', 'Empresa', 'NIT', 'Nro Fundaempresa'];
    }
    public static function infoGralBenReport()
    {
        return DB::table("beneficiarios_donacion")
            ->leftJoin('personas', 'personas.id', '=', 'beneficiarios_donacion.persona_id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'personas.departamento_extension_id')
            ->select('personas.nombre', 'personas.primer_apellido', 'personas.segundo_apellido', 'personas.carnet_identidad', 'departamentos.sigla', 'personas.telefono', 'beneficiarios_donacion.nombre as nomb')
            ->get();
    }
    public static function heading_infoGralBenReport()
    {
        return ['Nombre del representante', 'Primer apellido', 'Segundo Apellido', 'CI', 'Expedido', 'Telefono', 'Institución/Empresa'];
    }
    public static function guiaInternacionReport($fecha_ini, $fecha_fin)
    {
        return DB::table("personas")
            ->leftJoin('guias_internacion', 'personas.id', '=', 'guias_internacion.persona_id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'personas.departamento_extension_id')
            ->select('personas.nombre', 'personas.primer_apellido', 'personas.segundo_apellido', 'personas.carnet_identidad', 'departamentos.sigla', 'personas.telefono', 'guias_internacion.correlativo', 'guias_internacion.orden_comunal', 'guias_internacion.numero_taques', DB::raw("(CASE WHEN (guias_internacion.controlado = true) THEN 'Controlado' ELSE 'No controlado' END) as controlado"), DB::raw("(CASE WHEN (guias_internacion.retenido = true) THEN 'Retenido' ELSE 'No retenido' END) as retenido"))
            ->whereBetween(DB::raw('DATE(guias_internacion.updated_at)'), [$fecha_ini, $fecha_fin])
            ->get();
    }
    public static function heading_guiaInternacionReport()
    {
        return ['Nombres', 'Apellido Paterno', 'Apellido Materno', 'CI', 'Expedido', 'Teléfono/Celular', 'Correlativo', 'Orden comunal', 'Nro Taques', 'Control', 'Retención'];
    }
    public static function hojaRutaComReport($fecha_ini, $fecha_fin)
    {
        return DB::table('hoja_rutas')
            ->leftJoin('personas', 'personas.id', '=', 'hoja_rutas.persona_id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'personas.departamento_extension_id')
            ->select('personas.nombre', 'personas.primer_apellido', 'personas.segundo_apellido', 'personas.carnet_identidad', 'departamentos.sigla', 'personas.telefono', 'hoja_rutas.taques', 'hoja_rutas.codigo_comercializador', 'hoja_rutas.correlativo', DB::raw("(CASE WHEN (hoja_rutas.estado = false) THEN 'En Proceso' ELSE 'Finalizado' END) as estado"))
            ->where('personas.tipo_persona_id', 3)
            ->whereBetween(DB::raw('DATE(hoja_rutas.updated_at)'),  [$fecha_ini, $fecha_fin])
            ->get();
    }
    public static function heading_hojaRutaComReport()
    {
        return ['Nombres', 'Apellido Paterno', 'Apellido Materno', 'CI', 'Expedido', 'Teléfono/Celular', 'Correlativo', 'Orden comunal', 'Nro Taques', 'Control', 'Retención'];
    }
    public static function volGuiaIntReport($fecha_ini, $fecha_fin)
    {
        return DB::table("guias_internacion")
            ->select(DB::raw("SUM(guias_internacion.numero_taques) as total_taques"), DB::raw("SUM(guias_internacion.numero_taques)*50 as total_libras"))
            ->whereBetween(DB::raw('DATE(guias_internacion.updated_at)'), [$fecha_ini, $fecha_fin])
            ->get();
    }
    public static function heading_volGuiaIntReport()
    {
        return ['Total de taques', 'Total en libras'];
    }
    public static function volHojaRutaReport($iniDay, $finDay)
    {
        return DB::table("hoja_rutas")
            ->select(DB::raw("SUM(hoja_rutas.taques) as total_taques"), DB::raw("SUM(hoja_rutas.taques)*50 as total_libras"))
            ->whereBetween(DB::raw('DATE(hoja_rutas.updated_at)'), [$iniDay, $finDay])
            ->get();
    }
    public static function heading_volHojaRutaReport()
    {
        return ['Total de taques', 'Total en libras'];
    }
    public static function volCocaComReport($iniDay, $finDay)
    {
        return DB::table("puestos_de_control")
            ->select('puestos_de_control.nombre', DB::raw("SUM(guias_internacion.numero_taques) as total_taques"), DB::raw("SUM(guias_internacion.numero_taques)*50 as total_libras"))
            ->leftJoin('ruta_puesto', 'ruta_puesto.puesto_control_id', '=', 'puestos_de_control.id')
            ->leftJoin('rutas', 'rutas.id', '=', 'ruta_puesto.ruta_id')
            ->leftJoin('guias_internacion', 'guias_internacion.ruta_id', '=', 'rutas.id')
            ->groupBy("puestos_de_control.id")
            ->whereBetween(DB::raw('DATE(guias_internacion.updated_at)'), [$iniDay, $finDay])
            ->get();
    }
    public static function heading_volCocaComReport()
    {
        return ['Punto de control', 'Total de taques', 'Total en libras'];
    }
    public static function volCocaDepo($iniDay, $finDay)
    {
        $retenciones = DB::table("depositos")
            ->select(DB::raw("SUM(COALESCE(retenciones.numero_taques, 0)) as total_taques"), DB::raw("SUM(COALESCE(retenciones.numero_taques, 0))*50 as total_libras"))
            ->leftJoin('retenciones', 'retenciones.id', '=', 'depositos.retencion_id')
            ->whereBetween(DB::raw('DATE(retenciones.updated_at)'), [$iniDay, $finDay])
            ->get()->toArray();

        $devoluciones = DB::table("depositos")
            ->select(DB::raw("SUM(devoluciones.numero_taques) as total_taques"), DB::raw("SUM(devoluciones.numero_taques)*50 as total_libras"))
            ->leftJoin('devoluciones', 'devoluciones.deposito_id', '=', 'depositos.id')
            ->whereBetween(DB::raw('DATE(devoluciones.updated_at)'), [$iniDay, $finDay])
            ->get()->toArray();

        $depositado = DB::table("depositos")->select(DB::raw("SUM(depositos.numero_taques) as total_taques"), DB::raw("SUM(depositos.numero_taques)*50 as total_libras"))
            ->whereBetween(DB::raw('DATE(depositos.updated_at)'), [$iniDay, $finDay])
            ->get()->toArray();
        $query =
            [
                'cantidad_entrante_taques' => (is_null($retenciones[0]->total_taques)) ? '0' : $retenciones[0]->total_taques,
                'cantidad_entrante_libras' => (is_null($retenciones[0]->total_libras)) ? '0' : $retenciones[0]->total_libras,
                'cantidad_saliente_taques' => (is_null($devoluciones[0]->total_taques)) ? '0' : $devoluciones[0]->total_taques,
                'cantidad_saliente_libras' => (is_null($devoluciones[0]->total_libras)) ? '0' : $devoluciones[0]->total_libras,
                'cantidad_depositada' => (is_null($depositado[0]->total_taques)) ? '0' : $depositado[0]->total_taques,
                'cantidad_depositada_libras' => (is_null($depositado[0]->total_libras)) ? '0' : $depositado[0]->total_libras,
            ];
        return $query;
    }
    public static function heading_volCocaDepo()
    {
        return ['Cant. entrante-taques', 'Cant. entrante-taques - libras', 'Cant. saliente-taques', 'Cant. saliente-taques (libras)', 'Cant. depositada-taques', 'Cant. depositada-taques (libras)'];
    }
    public static function infoGralEmprReport($iniDay, $finDay)
    {
        return DB::table('hoja_rutas')
            ->leftJoin('personas', 'personas.id', '=', 'hoja_rutas.persona_id')
            ->leftJoin('empresas', 'empresas.persona_id', '=', 'personas.id')
            ->leftJoin('comercializador_infraccions', 'comercializador_infraccions.id', '=', 'personas.id')
            ->leftJoin('sanciones', 'sanciones.id', '=', 'comercializador_infraccions.sancion_id')
            ->leftJoin('resoluciones_administrativas', 'resoluciones_administrativas.persona_id', '=', 'personas.id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'personas.departamento_extension_id')
            ->select('personas.nombre', 'personas.primer_apellido', 'personas.segundo_apellido', 'personas.carnet_identidad', 'departamentos.sigla', 'personas.telefono', 'hoja_rutas.taques', 'hoja_rutas.codigo_comercializador', 'hoja_rutas.correlativo', DB::raw("(CASE WHEN (hoja_rutas.estado = false) THEN 'En Proceso' ELSE 'Finalizado' END) as estado"), 'sanciones.codigo', 'sanciones.monto', 'resoluciones_administrativas.resolucion', 'resoluciones_administrativas.observaciones', DB::raw("(CASE WHEN (resoluciones_administrativas.activo = true) THEN 'Cumplido' ELSE 'No establecido' END) as activo"))
            ->where('personas.tipo_persona_id', 4)
            ->whereBetween(DB::raw('DATE(hoja_rutas.updated_at)'), [$iniDay, $finDay])
            ->get();
    }
    public static function heading_infoGralEmprReport()
    {
        return ['Nombre del representante', 'Primer apellido', 'Segundo Apellido', 'Carnet', 'Expedido', 'Telefono/Celular', 'Taques', 'Código Comercializador', 'Correlativo', 'Estado hoja ruta', 'Codigo sanción', 'Monto sanción', 'Resolución administrativa', 'Observaciones', 'Estado resolución'];
    }
    public static function infoGralInvReport($iniDay, $finDay)
    {
        return DB::table('hoja_rutas')
            ->leftJoin('personas', 'personas.id', '=', 'hoja_rutas.persona_id')
            ->leftJoin('entidades_investigacion', 'entidades_investigacion.persona_id', '=', 'personas.id')
            ->leftJoin('comercializador_infraccions', 'comercializador_infraccions.id', '=', 'personas.id')
            ->leftJoin('sanciones', 'sanciones.id', '=', 'comercializador_infraccions.sancion_id')
            ->leftJoin('resoluciones_administrativas', 'resoluciones_administrativas.persona_id', '=', 'personas.id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'personas.departamento_extension_id')
            ->select('personas.nombre', 'personas.primer_apellido', 'personas.segundo_apellido', 'personas.carnet_identidad', 'departamentos.sigla', 'personas.telefono', 'hoja_rutas.taques', 'hoja_rutas.codigo_comercializador', 'hoja_rutas.correlativo', DB::raw("(CASE WHEN (hoja_rutas.estado = false) THEN 'En Proceso' ELSE 'Finalizado' END) as estado"), 'sanciones.codigo', 'sanciones.monto', 'resoluciones_administrativas.resolucion', 'resoluciones_administrativas.observaciones', DB::raw("(CASE WHEN (resoluciones_administrativas.activo = true) THEN 'Cumplido' ELSE 'No establecido' END) as activo"))
            ->where('personas.tipo_persona_id', 6)
            ->whereBetween(DB::raw('DATE(hoja_rutas.updated_at)'), [$iniDay, $finDay])
            ->get();
    }
    public static function heading_infoGralInvReport()
    {
        return ['Nombre del representante', 'Primer apellido', 'Segundo Apellido', 'Carnet', 'Expedido', 'Telefono/Celular', 'Taques', 'Código Comercializador', 'Correlativo', 'Estado hoja ruta', 'Codigo sanción', 'Monto sanción', 'Resolución administrativa', 'Observaciones', 'Estado resolución'];
    }
    public static function cantHojaEmpresaReport($iniDay, $finDay)
    {
        return DB::table('hoja_rutas')
            ->leftJoin('personas', 'personas.id', '=', 'hoja_rutas.persona_id')
            ->leftJoin('empresas', 'empresas.persona_id', '=', 'personas.id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'personas.departamento_extension_id')
            ->select('empresas.nombre as empresa', 'empresas.nit', 'personas.nombre', 'personas.primer_apellido', 'personas.segundo_apellido', 'personas.carnet_identidad', 'departamentos.sigla', 'personas.telefono', DB::raw("SUM(hoja_rutas.taques) as total_taques"), DB::raw("SUM(hoja_rutas.taques)*50 as total_libras"))
            ->where('personas.tipo_persona_id', 4)
            ->groupBy("hoja_rutas.id", "personas.id", "departamentos.id", "empresas.id")
            ->whereBetween(DB::raw('DATE(hoja_rutas.updated_at)'), [$iniDay, $finDay])
            ->get();
    }
    public static function heading_cantHojaEmpresaReport()
    {
        return ['Empresa', 'NIT', 'Nombres del representante', 'Primer apellido', 'Segundo Apellido', 'Carnet', 'Expedido', 'Telefono/Celular', 'Cantidad de taques', 'Cantidad de taques (libras)'];
    }
}
