<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CallCenter extends Model
{
    use SoftDeletes;
    protected $table = 'callcenters';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'persona_id', 'celular', 'telefono'
    ];
    
    /**
     * Definicion de relaciones.
     */
    public function persona() 
    {
        return $this->belongsTo(Persona::class);
    }        
}