<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EnlaceInstitucional extends Model
{
    use SoftDeletes;
    protected $table = 'enlaces_institucionales';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'enlace', 'descripcion'
    ];
}