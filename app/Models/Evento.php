<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evento extends Model
{
    use SoftDeletes;
    protected $table ='log_eventos_sistema';
    public $timestamps = true;
    public $guarded = ['id'];

    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable =['fecha','usuario','titulo','descripcion',
        'lista_adjunto','prioridad_id','estado'];

    /**
     * Definicion de relaciones.
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
