<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProyObraProductor extends Model
{
    use SoftDeletes;
    protected $table = 'proy_obras_productor';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_proy_obras_productor_id', 'nombre', 'descripcion', 'persona_id'
    ];
    
    /**
     * Definicion de relaciones.
     */
    public function tipo_proy_obras_productor() 
    {
        return $this->belongsTo(TipoProyObraProductor::class);
    }
    public function persona() 
    {
        return $this->belongsTo(Persona::class);
    }    
}