<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reclamo extends Model
{
    use SoftDeletes;
    protected $table = 'reclamos';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'problema', 'descripcion_problema', 'codigo_reclamo', 'fecha_nacimiento', 'carnet_identidad', 'email'
    ];
    
    public function persona() 
    {
        return $this->belongsTo(Persona::class);
    }       
}