<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Consulta extends Model
{
    use SoftDeletes;
    protected $table = 'consultas';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'asunto', 'contenido', 'fecha_nacimiento', 'carnet_identidad', 'email'
    ];
    
    public function persona() 
    {
        return $this->belongsTo(Persona::class);
    }       
}