<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipoEjecucion extends Model
{
    protected $table = 'tipo_ejecuciones';
    public $guarded = ['id'];
    //
    protected $fillable = [
        'id','descripcion_ejecucion'
    ];

    public function zona_racionalizada(){
        return $this->hasMany(zona_racionalizada::class);
    }
}
