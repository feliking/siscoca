<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProyObraProductor extends Model
{
    protected $table = 'tipo_proy_obras_productor';
    public $timestamps = true;
    public $guarded = ['id'];
    
    /**
     * Atributos asignables.
     *
     * @var array
     */
    protected $fillable = [
        'nombre'
    ];
    
    /**
     * Definicion de relacion de extension.
     */
    public function proy_obras_productor() 
    {
        return $this->hasMany(ProyObraProductor::class);
    }    
}