<?php

namespace App\Models\Notificaciones;

use Illuminate\Database\Eloquent\Model;

class RegistroEventos extends Model
{
    protected $table = 'registro_eventos';
    public $timestamps = true;
    public $guarded = ['id'];

    protected $fillable = [
        'evento_id', 'observaciones','detalle', 'user_id', 'hora_inicio', 'hora_fin', 'puesto_id'
    ];

    public function evento_exepcional() {
        return $this->belongsTo(EventosExepcionales::class);
    }
    public function puesto_control(){
        return $this->belongsTo(\App\PuestoControl::class);
    }
}
