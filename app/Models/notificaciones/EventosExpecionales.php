<?php

namespace App\Models\Notificaciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventosExepcionales extends Model
{
    use SoftDeletes;
    protected $table = 'eventos_exepcionales';
    public $timestamps =  true;
    public $guarded =['id'];

    protected $fillable = [
        'user_id', 'titulo', 'descripcion', 'fillable', 'prioridad'
    ];

    public function registroEventos() {
        return $this->hasMany(registroEventos::class);
    }
}
