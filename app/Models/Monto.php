<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monto extends Model
{
    protected $fillable = ['nombre', 'valor'];
    
}
