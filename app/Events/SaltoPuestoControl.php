<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Models\Notificaciones\RegistroEventos;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Auth;
use DB;

class SaltoPuestoControl implements ShouldBroadCast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $control_hoja;

    public function __construct($obj)
    {
        $evento = array(
        'evento_id' =>'1',
        'user_id' => Auth::id(),
        'hora_inicio' => date('Y-m-d H:m:s'),
        'hora_fin' => date('Y-m-d H:m:s'),
        'puesto_id' => $obj['puesto_id'],
        'detalle' => json_encode($obj['detalle']));
    
        $reg_evento = RegistroEventos::create($evento);
        $this->control_hoja = RegistroEventos::with('evento_exepcional','puesto_control')
                                        ->get();
                                        //->findOrFail($reg_evento->id);
        $this->control_hoja = DB::table('registro_eventos as re')
                                    ->join('eventos_exepcionales as ee','re.evento_id', 'ee.id')
                                    ->join('puestos_de_control as pc', 're.puesto_id', 'pc.id')
                                    ->join('users', 're.user_id', '=', 'users.id')
                                    ->where('re.id', '=',$reg_evento->id)
                                    ->select('re.*', 'ee.titulo as evento', 'pc.nombre as puesto_control', 'users.name as usuario')
                                    ->get()[0];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('eventos_exepcionales');
    }
}
