<?php

namespace App\Http\Controllers;
use App\Reportes;
use Carbon\Carbon;
use App\Http\Chunk\GenerateReport\generateReport;
use Illuminate\Http\Request;
class ReportePDFController extends Controller
{
    /**
     * Imprime un recurso especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reporte(Request $request)
    {
        $report = new generateReport;
        return $report->generarReporte('pdf', $request->query()); 
    }
    // COMERCIALIZACION
    public function hojaRutaReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');

        $query = $this->objectToArray(Reportes::hojaRuta($request['fechaini'], $request['fechafin'])->toArray());
        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' =>  Reportes::headingHojaRuta(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE HOJAS DE RUTA DIARIA'
        ];
        return $this->generatePDF('reportes.print', $data);
    }

    public function cantidadReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = Reportes::cantidadReport();
        $data = [
            'report' => $query,
            'keys' => array_keys($query),
            'heading' =>  Reportes::heading_cantidadReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE CANTIDAD DE PRODUCTORES Y COMERCIALIZADORES'
        ];
        return $this->generatePDF('reportes.print_cantidad', $data);
    }

    public function infoGralProdReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::infoGralProdReport($request['fechaini'], $request['fechafin'])->toArray());
        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_infoGralProdReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE INFORMACIÓN GENERAL DE PRODUCTORES'
        ];
        return $this->generatePDF('reportes.print', $data);
    }

    public function infoGralProdDetReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::infoGralProdDetReport($request['fechaini'], $request['fechafin'])->toArray());
        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_infoGralProdDetReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE INFORMACIÓN GENERAL DE PRODUCTORES AL DETALLE'
        ];
        return $this->generatePDF('reportes.print', $data);
    }

    public function infoGralComReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::infoGralComReport()->toArray());
        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_infoGralComReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE INFORMACIÓN GENERAL DE COMERCIALIZADORES'
        ];
        return $this->generatePDF('reportes.print', $data);
    }

    public function infoGralEmpReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::infoGralEmpReport()->toArray());
        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_infoGralEmpReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE INFORMACIÓN GENERAL DE EMPRESAS'
        ];
        return $this->generatePDF('reportes.print', $data);
    }

    public function infoGralBenReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::infoGralBenReport()->toArray());
        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_infoGralBenReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE INFORMACIÓN GENERAL DE INSTITUCIONES BENEFICIARIAS'
        ];
        return $this->generatePDF('reportes.print', $data);
    }

    public function guiaInternacionReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::guiaInternacionReport($request['fechaini'], $request['fechafin']));
        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_guiaInternacionReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE COMERCIALIZACIÓN DE GUÍAS DE INTERNACIÓN'
        ];
        return $this->generatePDF('reportes.print', $data);
    }

    public function hojaRutaComReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::hojaRutaComReport($request['fechaini'], $request['fechafin']));
        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_hojaRutaComReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE HOJA DE RUTA POR COMERCIALIZACIÓN'
        ];
        return $this->generatePDF('reportes.print', $data);
    }

    public function volGuiaIntReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::volGuiaIntReport($request['fechaini'], $request['fechafin']));
        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_volGuiaIntReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE VOLUMEN DE TRASPORTE DE HOJAS DE COCA CON GUIAS DE INTERNACIÓN'
        ];
        return $this->generatePDF('reportes.print', $data);
    }

    public function volHojaRutaReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::volHojaRutaReport($request['fechaini'], $request['fechafin']));

        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_volHojaRutaReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE VOLUMEN DE TRASPORTE DE HOJAS DE COCA CON HOJAS DE RUTA'
        ];
        return $this->generatePDF('reportes.print', $data);
    }

    public function volCocaComReport($request)
    {

        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::volCocaComReport($request['fechaini'], $request['fechafin']));
        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_volCocaComReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE VOLUMEN DE HOJAS DE COCA POR PUNTOS DE CONTROL'
        ];
        return $this->generatePDF('reportes.print', $data);
    }

    public function volCocaDepo($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = Reportes::volCocaDepo($request['fechaini'], $request['fechafin']);
        $data = [
            'report' => $query,
            'keys' => array_keys($query),
            'heading' => Reportes::heading_volCocaDepo(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE VOLUMEN DE HOJAS DE COCA DISPONIBLE, ENTRANTE Y SALIENTE EN DEPÓSITOS'
        ];
        return $this->generatePDF('reportes.print_cantidad', $data);
    }

    public function infoGralEmprReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::infoGralEmprReport($request['fechaini'], $request['fechafin'])->toArray());
        $data = [
            'report' => $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_infoGralEmprReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE INFORMACIÓN GENERAL DE EMPRESAS INDUSTRIALIZADAS'
        ];

        return $this->generatePDF('reportes.print', $data);
    }

    public function infoGralInvReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::infoGralInvReport($request['fechaini'], $request['fechafin'])->toArray());
        $data = [
            'report' =>  $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_infoGralInvReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE INFORMACIÓN GENERAL DE ENTIDADES DE INVESTIGACIÓN'
        ];
        return $this->generatePDF('reportes.print', $data);
    }
    public function cantHojaEmpresaReport($request)
    {
        $fecha = ($request['fechaini'] == $request['fechafin']) ? Carbon::parse(now())->format('d/m/Y') : Carbon::parse($request['fechaini'])->format('d/m/Y') . '-' . Carbon::parse($request['fechafin'])->format('d/m/Y');
        $query = $this->objectToArray(Reportes::cantHojaEmpresaReport($request['fechaini'], $request['fechafin'])->toArray());
        $data = [
            'report' =>  $query,
            'keys' => (count($query) > 0) ? array_keys($query[0]) : '',
            'heading' => Reportes::heading_cantHojaEmpresaReport(),
            'fecha' => $fecha,
            'title' => 'REPORTE DE CANTIDAD DE HOJA COMPRADA POR EMPRESA INDUSTRIALIZADORA'
        ];
        return $this->generatePDF('reportes.print', $data);
    }
    public function generatePDF($vista, $data)
    {
        $headerHtml = view()->make('partial.head')->render();
        $footerHtml = view()->make('partial.foot')->render();
        $pageMargins = [25, 20, 25, 30];
        $pageName = 'reporte.pdf';
        return \PDF::loadView($vista, $data)->setOrientation('landscape')
            ->setOption('header-html', $headerHtml)
            ->setOption('footer-html', $footerHtml)
            ->setOption('page-size', 'Letter')
            ->setOption('margin-top', $pageMargins[0])
            ->setOption('margin-right', '2mm')
            ->setOption('margin-bottom', $pageMargins[2])
            ->setOption('margin-left', $pageMargins[2])
            ->setOption('encoding', 'utf-8')
            ->stream($pageName);
    }

    public function objectToArray($d)
    {
        if (is_object($d)) {
            $d = get_object_vars($d);
        }
        if (is_array($d)) {
            return array_map(array($this, 'objectToArray'), $d);
        } else {
            return $d;
        }
    }
}
