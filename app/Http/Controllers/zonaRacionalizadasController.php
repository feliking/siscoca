<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\zona_racionalizada;

class zonasRacionalizadasController extends Controller
{
    public function index() {
        return DB::table('racionalizacion_erradicacion_diaria as rac')
                    ->join('unidad_dependientes', 'unidad_dependientes.id', '=', 'unidad_id')
                    ->select('rac.fecha_racionalizacion', 'unidad_dependientes.descripcion as unidad_nombre', 'rac.hectareas_afectadas')
                    ->get();
    }
    public function store(Request $req){

        $zona_id = DB::table('racionalizacion_erradicacion_diaria')->insertGetId([
            'fecha_racionalizacion'=> $req->input('fecha_racionalizacion'),
            'departamento_id'=> $req->input('departamento_id'),
            'regional_id'=> $req->input('regional_id'),
            'unidad_id'=> $req->input('unidad_id'),
            'parcela_id'=>$req->input('parcela_id'),
            'tipo_ejecucion_id'=>$req->input('tipo_ejecucion'),
            'hectareas_afectadas'=>$req->input('hectareas_afectadas')
        ]);
        DB::table('parcelas')
        ->where('id', $req->input('parcela_id'))
        ->update(['descripcion' => $req->input('descripcion'),
                    'racionalizado'=>true]);
        return $zona_id;
    }
}
