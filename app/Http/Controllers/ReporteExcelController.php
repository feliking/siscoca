<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteExport;
use App\Exports\ReporteCantidadExport;
use Carbon\Carbon;
use App\Reportes;
use App\Http\Chunk\GenerateReport\generateReport;
use Illuminate\Http\Request;

class ReporteExcelController extends Controller
{
    public function reporte(Request $request)
    {   
        $report = new generateReport;
        return $report->generarReporte('excel', $request->query()); 
    }
    // COMERCIALIZACION
    public function hojaRutaReport($request)
    {
        $query =  Reportes::hojaRuta($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::headingHojaRuta();
        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE HOJAS DE RUTA DIARIA CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))], $cabecera
            ];
        } else {
            $heading = [
                ['REPORTE DE HOJAS DE RUTA DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera
            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function cantidadReport($request)
    {
        $query = Reportes::cantidadReport();
        $cabecera = Reportes::heading_cantidadReport();
        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE CANTIDAD DE PRODUCTORES Y COMERCIALIZADORES CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))], $cabecera
            ];
        } else {
            $heading = [
                ['REPORTE DE CANTIDAD DE PRODUCTORES Y COMERCIALIZADORES DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera
            ];
        }
        return Excel::download(new ReporteCantidadExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function infoGralProdReport($request)
    {
        $query = Reportes::infoGralProdReport($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::heading_infoGralProdReport();
        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE INFORMACIÓN GENERAL DE PRODUCTORES CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))], $cabecera

            ];
        } else {
            $heading = [
                ['REPORTE DE INFORMACIÓN GENERAL DE PRODUCTORES DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera

            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function infoGralProdDetReport($request)
    {
        $query = Reportes::infoGralProdDetReport($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::heading_infoGralProdDetReport();
        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE INFORMACIÓN GENERAL DE PRODUCTORES AL DETALLE CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))], $cabecera
            ];
        } else {
            $heading = [
                ['REPORTE DE INFORMACIÓN GENERAL DE PRODUCTORES AL DETALLE DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $$cabecera
            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function infoGralComReport($request)
    {
        $query = Reportes::infoGralComReport();
        $cabecera = Reportes::heading_infoGralComReport();
        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE INFORMACIÓN GENERAL DE COMERCIALIZADORES CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))], $cabecera
            ];
        } else {
            $heading = [
                ['REPORTE DE INFORMACIÓN GENERAL DE COMERCIALIZADORES DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))],
                ['Nombre', 'Primer apellido', 'Segundo Apellido', 'CI', 'Expedido', 'Telefono', 'Código sanción', 'Monto', 'Resolución', 'Tipo'], $cabecera

            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function infoGralEmpReport($request)
    {
        $query = Reportes::infoGralEmpReport();

        $cabecera = Reportes::heading_infoGralEmpReport();

        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE INFORMACIÓN GENERAL DE EMPRESAS CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))],  $cabecera,
            ];
        } else {
            $heading = [
                ['REPORTE DE INFORMACIÓN GENERAL DE EMPRESAS DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera,
            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function infoGralBenReport($request)
    {
        $query =  Reportes::infoGralBenReport();
        $cabecera = Reportes::heading_infoGralBenReport();

        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE INFORMACIÓN GENERAL DE INSTITUCIONES BENEFICIARIAS CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))],  $cabecera,
            ];
        } else {
            $heading = [
                ['REPORTE DE INFORMACIÓN GENERAL DE INSTITUCIONES BENEFICIARIAS DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera,
            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function guiaInternacionReport($request)
    {
        $query = Reportes::guiaInternacionReport($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::heading_guiaInternacionReport();


        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE COMERCIALIZACIÓN DE GUÍAS DE INTERNACIÓN CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))],  $cabecera,
            ];
        } else {
            $heading = [
                ['REPORTE DE COMERCIALIZACIÓN DE GUÍAS DE INTERNACIÓN  DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera,
            ];
        }

        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function hojaRutaComReport($request)
    {
        $query = Reportes::hojaRutaComReport($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::heading_hojaRutaComReport();

        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE HOJA DE RUTA POR COMERCIALIZACIÓN CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))],  $cabecera,
            ];
        } else {
            $heading = [
                ['REPORTE DE HOJA DE RUTA POR COMERCIALIZACIÓN DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera,
            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function volGuiaIntReport($request)
    {
        $query =  Reportes::volGuiaIntReport($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::heading_volGuiaIntReport();

        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE VOLUMEN DE TRASPORTE DE HOJAS DE COCA CON GUIAS DE INTERNACIÓN CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))],  $cabecera,
            ];
        } else {
            $heading = [
                ['REPORTE DE VOLUMEN DE TRASPORTE DE HOJAS DE COCA CON GUIAS DE INTERNACIÓN DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera,
            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function volHojaRutaReport($request)
    {
        $query = Reportes::volHojaRutaReport($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::heading_volHojaRutaReport();
        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE VOLUMEN DE TRASPORTE DE HOJAS DE COCA CON HOJAS DE RUTA CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))],  $cabecera,
            ];
        } else {
            $heading = [
                ['REPORTE DE VOLUMEN DE TRASPORTE DE HOJAS DE COCA CON HOJAS DE RUTA DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera,
            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function volCocaComReport($request)
    {
        $query = Reportes::volCocaComReport($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::heading_volCocaComReport();

        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE VOLUMEN DE HOJAS DE COCA POR PUNTOS DE CONTROL CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))],  $cabecera,
            ];
        } else {
            $heading = [
                ['REPORTE DE VOLUMEN DE HOJAS DE COCA POR PUNTOS DE CONTROL DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera,
            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function volCocaDepo($request)
    {
        $query = Reportes::volCocaDepo($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::heading_volCocaDepo();
        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE VOLUMEN DE HOJAS DE COCA DISPONIBLE, ENTRANTE Y SALIENTE EN DEPÓSITOS CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))],  $cabecera,
            ];
        } else {
            $heading = [
                ['REPORTE DE VOLUMEN DE HOJAS DE COCA DISPONIBLE, ENTRANTE Y SALIENTE EN DEPÓSITOS DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera,
            ];
        }
        return Excel::download(new ReporteCantidadExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }
    // INDUSTRIALIZACION
    public function infoGralEmprReport($request)
    {
        $query = Reportes::infoGralEmprReport($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::heading_infoGralEmprReport();
        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE INFORMACIÓN GENERAL DE EMPRESAS INDUSTRIALIZADAS CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))],  $cabecera,
            ];
        } else {
            $heading = [
                ['REPORTE INFORMACIÓN GENERAL DE EMPRESAS INDUSTRIALIZADAS DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera,
            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }

    public function infoGralInvReport($request)
    {
        $query = Reportes::infoGralInvReport($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::heading_infoGralInvReport();
        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE INFORMACIÓN GENERAL DE ENTIDADES DE INVESTIGACIÓN CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))],  $cabecera,
            ];
        } else {
            $heading = [
                ['REPORTE INFORMACIÓN GENERAL DE ENTIDADES DE INVESTIGACIÓN DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera,
            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }
    public function cantHojaEmpresaReport($request)
    {
        $query = Reportes::cantHojaEmpresaReport($request['fechaini'], $request['fechafin']);
        $cabecera = Reportes::heading_cantHojaEmpresaReport();
        if ($request['fechaini'] == $request['fechafin']) {
            $heading = [
                ['REPORTE DE CANTIDAD DE HOJA COMPRADA POR EMPRESA INDUSTRIALIZADORA CON FECHA ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y'))],  $cabecera,
            ];
        } else {
            $heading = [
                ['REPORTE DE CANTIDAD DE HOJA COMPRADA POR EMPRESA INDUSTRIALIZADORA DESDE ' . strtoupper(Carbon::parse($request['fechaini'])->format('d/M/Y')) . " HASTA " . strtoupper(Carbon::parse($request['fechafin'])->format('d/M/Y'))], $cabecera,
            ];
        }
        return Excel::download(new ReporteExport($heading, $query), 'Reporte-' . date('Y-m-d') . '.xlsx');
    }      
}
