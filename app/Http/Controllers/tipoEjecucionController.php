<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tipoEjecucion;

class tipoEjecucionController extends Controller
{
    public function index() 
    {
        return tipoEjecucion::get();
    }
}
