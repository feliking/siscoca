<?php

namespace App\Http\Controllers\DesarrolloIntegral;

use App\Models\DesarrolloIntegral\EvaluacionViabilidad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EvaluacionViabilidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EvaluacionViabilidad::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return EvaluacionViabilidad::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return EvaluacionViabilidad::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $evaluacion = EvaluacionViabilidad::findOrFail($id);
        $evaluacion->fill($request->all());
        $evaluacion->save();
        return $evaluacion;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $evaluacion = EvaluacionViabilidad::findOrFail($id);
        $evaluacion->delete();
        return $evaluacion;   
    }
    public function fill($request)
    {
        $request =json_decode($request,true);
        return EvaluacionViabilidad::with('proyectos')->where($request)->get();
    }
}
