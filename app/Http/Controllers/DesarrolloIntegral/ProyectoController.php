<?php

namespace App\Http\Controllers\DesarrolloIntegral;

use Illuminate\Http\Request;
use App\Models\DesarrolloIntegral\Proyecto;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProyectoController extends Controller
{
    /**
     * Display all resource
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        return Proyecto::with('priorizacions','supervisions','evaluacionviabilidads','convocatoria','municipio','solicitudAprobada', 'evaluacion_cofinanciamientos', 'ejecucion_proyecto')->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        return Proyecto::findOrFail($id);
    }

    /**
     * Store a newly Item if no one found in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {
        return Proyecto::create($request->all());         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) 
    {
        $municipio = Proyecto::findOrFail($id);
        $municipio->fill($request->all());
        $municipio->save();
        return $municipio;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $municipio = Proyecto::findOrFail($id);
        $municipio->delete();
        return $municipio;
    }

    /**
     * Lista recursos especificos por criterio
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fill($request) {
        $request = json_decode($request, true);
        return Proyecto::where($request)->get();
    }
    public function search(Request $req) {
        return DB::table('proyectos')
        ->where('nombre','like', $req->query('arg').'%')
        ->get(); 
    }
    public function priorizacionfill() 
    {
        return  $results = DB::select('select distinct proyectos.id as idproyecto, proyectos.*, convocatorias.*, priorizacions.*
        from proyectos
        inner join convocatorias on proyectos.convocatoria_id=convocatorias.id
        left join priorizacions on proyectos.id=priorizacions.proyecto_id 
        where convocatorias.deleted_at is null and priorizacions.deleted_at is null 
        and convocatorias.tipo_proyecto = 2');
    }

    public function showfill() 
    {
        return  $results = DB::select('select distinct proyectos.id as idproyecto, 
        proyectos.*, convocatorias.*, evaluacion_viabilidads.*, municipios.nombre as municipio
        from proyectos 
        inner join convocatorias on proyectos.convocatoria_id=convocatorias.id
        inner join municipios on proyectos.municipio_id=municipios.id
        left join evaluacion_viabilidads on proyectos.id=evaluacion_viabilidads.proyecto_id 
        where proyectos.deleted_at is null and evaluacion_viabilidads.deleted_at is null 
        and convocatorias.tipo_proyecto = 2');
    }

    public function supervisionfill() 
    {
        return  $results = DB::select('select distinct proyectos.id as idproyecto,proyectos.*,convocatorias.*,
        evaluacion_viabilidads.*, supervisions.* 
        from proyectos
        inner join convocatorias on proyectos.convocatoria_id=convocatorias.id
        inner join evaluacion_viabilidads on proyectos.id=evaluacion_viabilidads.proyecto_id 
        left join supervisions on proyectos.id=supervisions.proyecto_id 
        where convocatorias.deleted_at is null and evaluacion_viabilidads.deleted_at is null
        and supervisions.deleted_at is null
        and convocatorias.tipo_proyecto = 2 and evaluacion_viabilidads.estado<>false');
    }

    public function cierreobrafill() 
    {
        return  $results = DB::select('select distinct proyectos.id as idproyecto,proyectos.*, convocatorias.*,
        supervisions.*, cierre_obras.id as idcierre, cierre_obras.* 
        from proyectos
        inner join convocatorias on proyectos.convocatoria_id=convocatorias.id
        inner join supervisions on proyectos.id=supervisions.proyecto_id 
        left join cierre_obras on proyectos.id=cierre_obras.proyecto_id         
        where proyectos.deleted_at is null and supervisions.deleted_at is null
        and convocatorias.tipo_proyecto = 2');
    }

    public function evaluacionfill() 
    {
        return $results = DB::select('select distinct proyectos.id as idproyecto, proyectos.*,
         convocatorias.*, cierre_obras.*,
        evaluacion_impactos_oii.id as idevaluacion,evaluacion_impactos_oii.*
        from proyectos
        inner join convocatorias on proyectos.convocatoria_id=convocatorias.id
        left join cierre_obras on proyectos.id=cierre_obras.proyecto_id  
        left join evaluacion_impactos_oii on proyectos.id=evaluacion_impactos_oii.proyecto_id
        where convocatorias.deleted_at is null and cierre_obras.deleted_at is null 
        and evaluacion_impactos_oii.deleted_at is null and convocatorias.tipo_proyecto = 2');
    }
}
