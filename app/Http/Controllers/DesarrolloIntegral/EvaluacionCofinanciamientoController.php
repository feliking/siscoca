<?php

namespace App\Http\Controllers\DesarrolloIntegral;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DesarrolloIntegral\EvaluacionCofinanciamientos;

class EvaluacionCofinanciamientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EvaluacionCofinanciamientos::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return EvaluacionCofinanciamientos::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return EvaluacionCofinanciamientos::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $evaluacion =  EvaluacionCofinanciamientos::findOrFail($id);
        $evaluacion->fill($request->all());
        $evaluacion->save();
        return $evaluacion;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $evaluacion = EvaluacionCofinanciamientos::findOrFail($id);
        $evaluacion->delete();
        return $evaluacion;
    }
}
