<?php

namespace App\Http\Controllers\DesarrolloIntegral;

use App\Models\DesarrolloIntegral\Convocatoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;


class ConvocatoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Convocatoria::with('solicitudes')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Convocatoria::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Convocatoria::with('requisitos')->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $convocatoria = Convocatoria::findOrFail($id);
        $convocatoria->fill($request->all());
        $convocatoria->save();
        return $convocatoria;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $convocatoria = Convocatoria::findOrFail($id);
        $convocatoria->delete();
        return $convocatoria;
    }
    /**
     * Lista un recurso especifico por criterio
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fill($request)     {
        $request = json_decode($request, true);
        return Convocatoria::with('tipo_convocatoria','requisitos','solicitudes.convocatoria.proyecto'     
        )->where($request)->get();
    }

    

}
