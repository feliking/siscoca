<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DesarrolloIntegral\Capacitacion;

class CapacitacionesController extends Controller
{
    public function index() {
        return Capacitacion::with('conclusionCapacitacion')->get();
    }
    public function store(Request $request) 
    {
        return Capacitacion::create($request->all());
    }
    public function fullSearch(Request $request) {
        return Capacitacion::search($request->query('search'))->get();
    }
}
