<?php

namespace App\Http\Controllers\DesarrolloIntegral;

use App\Models\DesarrolloIntegral\TipoConvocatoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TipoConvocatoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TipoConvocatoria::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return TipoConvocatoria::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return TipoConvocatoria::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipo_convocatoria = TipoConvocatoria::findOrFail($id);
        $tipo_convocatoria->fill($request->all());
        $tipo_convocatoria->save();
        return $tipo_convocatoria;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipo_convocatoria = TipoConvocatoria::findOrFail($id);
        $tipo_convocatoria->delete();
        return $tipo_convocatoria;
    }

    public function fill($request) 
    {
        $request = json_decode($request, true);
        return TipoConvocatoria::where($request)->get();
    }
}
