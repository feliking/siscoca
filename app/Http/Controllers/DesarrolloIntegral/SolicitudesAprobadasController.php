<?php

namespace App\Http\Controllers\DesarrolloIntegral;

use Illuminate\Http\Request;
use App\Models\DesarrolloIntegral\SolicitudAprobada;
use App\Models\DesarrolloIntegral\seguimientoProyecto;
use App\Models\DesarrolloIntegral\reprogramacionProyecto;
use App\Models\DesarrolloIntegral\RecepcionProyecto;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SolicitudesAprobadasController extends Controller
{
    public function index(){
        return SolicitudAprobada::with('proyecto', 'reprogramacion', 'solicitud', 'proyecto.seguimientoproyecto', 'proyecto.municipio')->get();
    }

    public function saveSeguimiento(Request $req, $proyecto_id) {
        $resp = seguimientoProyecto::create($req->all());
        return $resp;
    }

    public function saveReprogramacion(Request $req, $id) {
        $resp = reprogramacionProyecto::create($req->all());
        return $resp;
    }

    public function saveRecepcion(Request $req, $id) {
        $resp = RecepcionProyecto::create($req->all());
        DB::table('proyectos')
        ->where('id', $resp->proyecto_id)
        ->update(['recepcion_id'=>$resp->id]);
        return $resp;
    }

    public function saveOrden(Request $req, $id) {
        $orden_id = DB::table('orden_proceder_proyectos')
        ->insertGetId([
            'fecha_emision'=> $req->input('fecha_emision'),
            'responsable_nombre'=> $req->input('responsable_nombre'),
            'observaciones'=> $req->input('observaciones'),
            'solicitud_aprobada_id'=> $req->input('solicitud_aprobada_id')
        ]);

        DB::table('solicitud_aprobadas')
            ->where('id', $req->input('solicitud_aprobada_id'))
            ->update(['orden_id'=>$orden_id]);
        return $orden_id;
    }
    public function saveCancelacion(Request $req, $id) {
        $resp = cancelacionProyecto::create($req->all());
        DB::table('proyectos')
        ->where('id', $resp->proyecto_id)
        ->update(['cancelacion_id'=>$resp->id]);
        return $resp;
    }

    public function store(Request $request){
        return SolicitudAprobada::create($request->all());
    }

    public function show($id){
        return SolicitudAprobada::findOrFail($id);
    }

    public function update(Request $request, $id){
        $solicitud = SolicitudAprobada::findOrFail($id);
        $solicitud->fill($request);
        $solicitud->save();
        return $solicitud;
    }

    public function destroy($id){
        $solicitud = SolicitudAprobada::findOrFail($id);
        $solicitud->delete();
        return $solicitud;
    }

    public function fill(Request $request){
        $request = json_decode($request, true);
        return SolicitudAprobada::where($request)->get();
    }
}
