<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\DesarrolloIntegral\ConclusionCapacitacion;

class conclusionCapacitacionController extends Controller
{
    public function store(Request $request, $id) {
        $resp = ConclusionCapacitacion::create($request->all());
        DB::table('capacitaciones')
        ->where('id', $id)
        ->update(['conclusion_id'=> $resp->id]);
        return $resp;
    }
}
