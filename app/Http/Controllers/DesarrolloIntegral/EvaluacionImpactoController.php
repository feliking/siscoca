<?php

namespace App\Http\Controllers\DesarrolloIntegral;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DesarrolloIntegral\EvaluacionImpacto;

class EvaluacionImpactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EvaluacionImpacto::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return EvaluacionImpacto::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return EvaluacionImpacto::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $evaluacion = EvaluacionImpacto::findOrFail($id);
        $evaluacion->fill($request->all());
        $evaluacion->save();
        return $evaluacion;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $evaluacion = EvaluacionImpacto::findOrFail($id);
        $evaluacion->delete();
        return $evaluacion;
    }
     /**
     * Lista un recurso especifico por criterio
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fill($request)     {
        $request = json_decode($request, true);
        return EvaluacionImpacto::with('proyecto')->where($request)->get();

    }
}
