<?php

namespace App\Http\Controllers\DesarrolloIntegral;

use App\Models\DesarrolloIntegral\Solicitud;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Solicitud::with('convocatoria', 'propuestas', 'propuestas.solicitud.convocatoria.proyecto', 'propuestas.solicitud.convocatoria.requisitos', 'dictamen_apertura_sobres')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Solicitud::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Solicitud::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $solicitud = Solicitud::findOrFail($id);
        $solicitud->fill($request->all());
        $solicitud->save();
        return $solicitud;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $solicitud = Solicitud::findOrFail($id);
        $solicitud->delete();
        return $solicitud;
    }

    public function fill($request) 
    {
        $request = json_decode($request, true);
        return Solicitud::with('propuestas', 'convocatoria')->where($request)->get();
    }

    public function print($id){
        // $headerHtml = view()->make('partial.head')->render();
        // $footerHtml = view()->make('solicitud.foot')->render();
        // $pageMargins = [30, 10, 40, 10];
        // $pageName = 'solicitud_oii.pdf';
        // $hoja_ruta = Solicitud::with('convocatoria')->find($id);
        // for($i = 0; $i < count($hoja_ruta->ruta->puestos_de_control); $i++){
        //     $temp = $hoja_ruta->ruta->puestos_de_control[$i];
        //     $hash = base64_encode($temp->nombre.$hoja_ruta->correlativo.$hoja_ruta->persona->carnet_identidad.$hoja_ruta->persona->nombre.$hoja_ruta->persona->primer_apellido);
        //     $hoja_ruta->ruta->puestos_de_control[$i]->hash = $hash;
        // }
        // $data = [
        //     'solicitud' => $hoja_ruta
        // ];      
        // //dd($data);  
        // return \PDF::loadView('solicitud_oii.print', $data)
        // ->setOption('page-height', 345)
        // ->setOption('page-width', 216)
        // ->setOption('margin-top', $pageMargins[0])
        // ->setOption('margin-right', $pageMargins[1])
        // ->setOption('margin-bottom', $pageMargins[2])
        // ->setOption('margin-left', $pageMargins[3])
        // ->setOption('encoding', 'utf-8')
        // ->stream($pageName);
    }
}
