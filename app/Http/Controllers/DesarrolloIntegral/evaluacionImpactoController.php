<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DesarrolloIntegral;
use App\Http\Chunk\UploadFiles\uploadImages;
use Illuminate\Support\Facades\DB;

class evaluacionImpactoController extends Controller
{
    public function store(Request $req) {
        $uploadFiles = uploadImages::UploadBase64Images($req->input('images'));
        $evaluacion_id = DB::table('evaluacion_impactos')
        ->insertGetId([
            'fecha_evaluacion'=>$req->input('fecha_evaluacion'),
            'informe'=>$req->input('informe'),
            'observaciones'=>$req->input('observaciones'),
            'capacitacion_id'=>$req->input('capacitacion_id'),
            'adjuntos'=>json_encode($uploadFiles)
        ]);
        DB::table('capacitaciones')
        ->where('id', $req->input('capacitacion_id'))
        ->update(['evaluacion_id'=>$evaluacion_id]);
        return $evaluacion_id;
    }
}
