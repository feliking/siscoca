<?php

namespace App\Http\Controllers\DesarrolloIntegral;

use App\Models\DesarrolloIntegral\Propuesta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Propuesta::with('solicitud', 'solicitud.convocatoria.proyecto', 'solicitud.convocatoria.requisitos')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Propuesta::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Propuesta::with('solicitud')->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $propuesta = Propuesta::findOrFail($id);
        $propuesta->fill($request->all());
        $propuesta->save();
        return $propuesta;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $propuesta = Propuesta::with('solicitud')->findOrFail($id);
        $propuesta->delete();
        return $propuesta;
    }
}
