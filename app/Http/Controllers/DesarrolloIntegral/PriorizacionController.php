<?php

namespace App\Http\Controllers\DesarrolloIntegral;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DesarrolloIntegral\Priorizacion;


class PriorizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Priorizacion::with('convocatoria')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Priorizacion::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Priorizacion::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $priorizacion = Priorizacion::findOrFail($id);
        $priorizacion->fill($request->all());
        $priorizacion->save();
        return $priorizacion;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $priorizacion = Priorizacion::findOrFail($id);
        $priorizacion->delete();
        return $priorizacion;
    }
    
    /**
     * Lista un recurso especifico por criterio
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fill($request)     {
        $request = json_decode($request, true);
        return Priorizacion::with('convocatoria')->where($request)->get();

    }
}
