<?php

namespace App\Http\Controllers\DesarrolloIntegral;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DesarrolloIntegral\EjecucionProyecto;

class EjecucionProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EjecucionProyecto::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return EjecucionProyecto::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return EjecucionProyecto::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proyecto = EjecucionProyecto::findOrFail($id);
        $proyecto->fill($request->all());
        $proyecto->save();
        return $proyecto;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proyecto = EjecucionProyecto::findOrFail($id);
        $proyecto->delete();
        return $proyecto;
    }
}
