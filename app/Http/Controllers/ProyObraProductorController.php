<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ProyObraProductor;

class ProyObraProductorController extends Controller
{
    /**
     * Display all resource
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        return ProyObraProductor::with('tipo_proy_obras_productor', 'persona')->get();        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        return ProyObraProductor::findOrFail($id);
    }

    /**
     * Store a newly Item if no one found in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {
        return ProyObraProductor::create($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) 
    {
        $proyObraProductor = ProyObraProductor::findOrFail($id);
        $proyObraProductor->fill($request->all());
        $proyObraProductor->save();
        return $proyObraProductor;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $proyObraProductor = ProyObraProductor::findOrFail($id);
        $proyObraProductor->delete();
        return $proyObraProductor;
    }

    /**
     * Lista un recurso especifico por criterio
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fill($request) 
    {
        $request = json_decode($request, true);
        return ProyObraProductor::where($request)->get();
    }
}
