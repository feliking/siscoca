<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Persona;

class PersonaController extends Controller
{
    /**
     * Lista los recursos
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {   
        return Persona::with('tipo_persona', 'departamento_extension', 'pais_nacimiento', 'departamento_nacimiento', 'provincia_nacimiento', 'persona', 'persona.departamento_extension', 'sustitucion', 'cesaciones', 'resoluciones_administrativas', 'puestos_venta', 'hoja_rutas', 'guia_internacions')->orderBy('id', 'desc')->get()->take(100);

    }

    /**
     * Lista un recurso especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        return Persona::with('tipo_persona', 'departamento_extension', 'pais_nacimiento', 'departamento_nacimiento', 'provincia_nacimiento', 'persona', 'persona.departamento_extension', 'sustitucion', 'cesaciones', 'resoluciones_administrativas')->findOrFail($id);
    }

    public function productorSearch(Request $request) {
        return DB::table('personas')
                    ->join('tipos_persona', 'personas.tipo_persona_id', '=', 'tipos_persona.id')
                    ->where([
                        ['personas.carnet_identidad', 'like', '%'.$request->query('carnet').'%'],
                        ['tipo_persona_id', '<', '3']
                    ])
                    ->select('personas.*')
                    ->get();
    }

    /**
     * Guarda un nuevo recurso
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {
        $request['codigo'] = $this->setMax($request);
        return Persona::create($request->all());
    }

    /**
     * Actualiza un recurso especifico
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) 
    {
        $data = Persona::findOrFail($id);
        $data->fill($request->all());
        $data->save();
        return $data;
    }

    /**
     * Borra un recurso especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $data = Persona::findOrFail($id);
        $data->delete();
    }

    /**
     * Lista un recurso especifico por criterio
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function fill($request) 
    {
        $request = json_decode($request, true);

        return Persona::with('poder', 'tipo_persona', 'departamento_extension', 'pais_nacimiento', 'departamento_nacimiento', 'provincia_nacimiento', 'persona', 'persona.departamento_extension', 'sustitucion', 'cesaciones', 'carnet_productor', 'carnet_comercializador', 'resoluciones_administrativas','resoluciones_administrativas.tipo_resolucion', 'hoja_rutas', 'guia_internacions', 'beneficiario_donacions')
            ->where($request)->orderBy('id', 'desc')->get()->take(100);
    }

    public function hoja_ruta(){
        return Persona::with('poder', 'tipo_persona', 'departamento_extension', 'pais_nacimiento', 'departamento_nacimiento', 'provincia_nacimiento', 'persona', 'persona.departamento_extension', 'sustitucion', 'cesaciones', 'carnet_productor', 'carnet_comercializador', 'resoluciones_administrativas','resoluciones_administrativas.tipo_resolucion')
        ->whereBetween('tipo_persona_id', [1,6])->get();
    }    

    public function setMax($request){
        $correlativo = '';
        switch ($request->tipo_persona_id) {
            case 1:
                $correlativo = 'PRO';
                break;
            case 2:
                $correlativo = 'PRD';
                break;
            case 3:
                $correlativo = 'COM';
                break;
            case 4:
                $correlativo = 'IND';
                break;
            case 5:
                $correlativo = 'INV';
                break;
            case 6:
                $correlativo = 'DON';
                break;
            case 7:
                $correlativo = 'DEI';
                break;
        }
        if(Persona::where('tipo_persona_id', $request->tipo_persona_id)->count() > 0){
            $latest = Persona::orderBy('created_at', 'DESC')->take(1)->where('tipo_persona_id', $request->tipo_persona_id)->get();
            $max = explode('-', $latest[0]->codigo);
            $correlativo = $correlativo.'-'.str_pad($max[1] + 1, 7, "0", STR_PAD_LEFT);
            return $correlativo;
        }
        else{     
            $correlativo = $correlativo.'-'.str_pad('1', 7, "0", STR_PAD_LEFT);
            return $correlativo;
        }
    }

    // Esta función es de acceso publico, así que solo debe exponerse la información a desplegar
    // No debe exponerse información delicada de los actores del sistema por seguridad ;)
    public function quest(Request $request) 
    {
        return Persona::with(
            'poder', 
            'tipo_persona', 
            'departamento_extension', 
            'pais_nacimiento', 
            'departamento_nacimiento', 
            'provincia_nacimiento', 
            'persona', 
            'persona.departamento_extension', 
            'hoja_rutas', 
            'guia_internacions.persona.departamento_extension',
            'guia_internacions.control_guia_internacion',
            'guia_internacions.retencion',
            'carnet_productor',
            'carnet_comercializador',
            'hoja_rutas.persona.departamento_extension')
            ->where($request->all())->get();
    }
}
