<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CarnetComercializador;
use App\Firma;

class CarnetComercializadorController extends Controller
{
    /**
     * Lista los recursos
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        return CarnetComercializador::with('persona', 'persona.departamento_extension')->get();
    }

    /**
     * Lista un recurso especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        return CarnetComercializador::findOrFail($id);
    }

    /**
     * Guarda un nuevo recurso
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {
        return CarnetComercializador::create($request->all());
    }

    /**
     * Actualiza un recurso especifico
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) 
    {
        $data = CarnetComercializador::findOrFail($id);
        $data->fill($request->all());
        $data->save();
    }

    /**
     * Borra un recurso especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $data = CarnetComercializador::findOrFail($id);
        $data->delete();
    }

    /**
     * Lista un recurso especifico por criterio
     *
     * @param  string
     * @return \Illuminate\Http\Response
     */
    public function fill($request) 
    {
        $request = json_decode($request, true);
        return CarnetComercializador::with('persona', 'persona.departamento_extension')->where($request)->orderBy('created_at', 'DESC')->get();
    }

    /**
     * Imprime un recurso especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print($id) {
        $pageSize = [87, 55];
        $pageMargins = [0, 0, 0, 0];
        $pageName = 'carnet_comercializador.pdf';
        $data = [
            'carnet_comercializador' => CarnetComercializador::with('persona', 'persona.departamento_extension', 'persona.tipo_persona')
                                    ->find($id),
            'firma_viceministro' => Firma::findOrFail(1),
            'firma_director' => Firma::findOrFail(2)
        ];        
        return \PDF::loadView('carnet_comercializador.print', $data)
        // ->setOption('page-size','Letter')
        ->setOption('page-width', $pageSize[0])
        ->setOption('page-height', $pageSize[1])
        ->setOption('margin-top', $pageMargins[0])
        ->setOption('margin-right', $pageMargins[1])
        ->setOption('margin-bottom', $pageMargins[2])
        ->setOption('margin-left', $pageMargins[3])
        ->setOption('encoding', 'utf-8')
        ->stream($pageName);
    }
}
