<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Normativa;

class NormativaController extends Controller
{
    /**
     * Display all resource
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        return Normativa::with('tipo_normativa')->get();        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        return Normativa::findOrFail($id);
    }

    /**
     * Store a newly Item if no one found in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {
        return Normativa::create($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) 
    {
        $normativa = Normativa::findOrFail($id);
        $normativa->fill($request->all());
        $normativa->save();
        return $normativa;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $normativa = Normativa::findOrFail($id);
        $normativa->delete();
        return $normativa;
    }

    /**
     * Lista un recurso especifico por criterio
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fill($request) 
    {
        $request = json_decode($request, true);
        return Normativa::where($request)->get();
    }
}
