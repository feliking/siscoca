<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\unidad_dependiente;

class unidadesDependientesController extends Controller
{
    public function index() 
    {
        return unidad_dependiente::get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        return unidad_dependiente::findOrFail($id);
    }

    /**
     * Store a newly Item if no one found in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {
        return unidad_dependiente::create($request->all());         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) 
    {
        $department = unidad_dependiente::findOrFail($id);
        $department->fill($request->all());
        $department->save();
        return $department;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $department = unidad_dependiente::findOrFail($id);
        $department->delete();
        return $department;
    }

    /**
     * Lista recursos especificos por criterio
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fill($request) 
    {
        $request = json_decode($request, true);
        return unidad_dependiente::where($request)->get();
    }
}
