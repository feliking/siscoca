<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Reclamo;

class ReclamoController extends Controller
{
    /**
     * Display all resource
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        return Reclamo::get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        return Reclamo::findOrFail($id);
    }

    /**
     * Store a newly Item if no one found in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {   
        $now = now();
        $codigo_reclamo = "COD-REC".$now->timestamp;        
        $request->request->add(['codigo_reclamo' => $codigo_reclamo]);
        return Reclamo::create($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) 
    {
        $reclamo = Reclamo::findOrFail($id);
        $reclamo->fill($request->all());
        $reclamo->save();
        return $reclamo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $reclamo = Reclamo::findOrFail($id);
        $reclamo->delete();
        return $reclamo;
    }

    /**
     * Lista un recurso especifico por criterio
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fill($request) 
    {
        $request = json_decode($request, true);
        return Reclamo::where($request)->get();
    }
}
