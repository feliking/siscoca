<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notificaciones\EventosExepcionales;
use Illuminate\Facades\Support\Auth;

class NotificacionesController extends Controller
{
    public function index() {
        return EventosExepcionales::get();
    }
    public function store(Request $request) {
        return EventosExepcionales::create($request->all());
    }
}
