<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notificaciones\RegistroEventos;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RegistroEventosController extends Controller
{
    function index(){
        return DB::table('registro_eventos as re')
                    ->join('eventos_exepcionales as ee', 'ee.id', '=', 're.evento_id')
                    ->join('puestos_de_control as pc', 're.puesto_id', '=', 'pc.id')
                    ->join('users', 're.user_id', '=', 'users.id')
                    ->select('re.*', 'ee.titulo as evento', 'pc.nombre as puesto_control', 'users.name as usuario')
                    ->latest()
                    ->get();
    }
    function getRegistroByUser(){
        $user_id = Auth::id();
        return DB::table('registro_eventos as re')
                    ->join('eventos_exepcionales as ee', 'ee.id', '=', 're.evento_id')
                    ->join('puestos_de_control as pc', 're.puesto_id', '=', 'pc.id')
                    ->where('re.user_id', '=', $user_id)
                    ->get();
    }
    function store(Request $request) {
        $registro = $request->all();
        $registro['user_id'] = Auth::id();
        $new_registro = RegistroEventos::create($registro);
        return $new_registro;
    }
}
