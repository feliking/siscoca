<?php

namespace App\Http\Controllers\ApiServices;

use Illuminate\Http\Request;
use App\Models\ApiServices\RegistroApiKey;
use Illuminate\Foundation\Auth\AuthenticasUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Tymon\JWTAuth\Claims\Issuer;
use Tymon\JWTAuth\Claims\IssuedAt;
use Tymon\JWTAuth\Claims\Expiration;
use Tymon\JWTAuth\Claims\NotBefore;
use Tymon\JWTAuth\Claims\JwtId;
use Tymon\JWTAuth\Claims\Subject;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use DB;

class ApiKeyGenerateController extends Controller
{
    public function create(Request $request) {

        $data = $request->all();
        $user = DB::table('users')
                    ->where('id', 1)
                    ->get()[0];
        $expiration = Carbon::now('UTC')->addYear();
        $jwt_data = array(
            'user_id' => $user->id,
            'institucion_id' => 1,//$request->input('institucion_id'),
            'email' => $user->email,
            'iss' => new Issuer('faker'),
            'iat' => new IssuedAt(Carbon::now('UTC')),
            'exp' => new Expiration($expiration),
            'nbf' => new NotBefore(Carbon::now('UTC')),
            'sub' => new Subject('faker'),
            'jti' => new JwtId('faker')
        );
        $customClaims = JWTFactory::customClaims($jwt_data);
        $payload = JWTFactory::make($customClaims);
        $token = JWTAuth::encode($payload);
        //return $token;
        $entidad_id = DB::table('entidad_servicios')
                            ->where('user_id', Auth::id())
                            ->select('id')
                            ->get()[0];
        $reg = RegistroApiKey::create([
            'apiKey' => $token,
            'fecha_finalizacion' => $expiration,
            'entidad_id' => $entidad_id->id
        ]);
        DB::table('entidad_servicios')
            ->where('id', $entidad_id->id)
            ->update(['ip_publica'=> $request->input('ip_address')]);
        return RegistroApiKey::findOrFail($reg->id);
    }

    public function getApiKeys (Request $request){
        return DB::table('registro_api_keys as reg')
                    ->join('entidad_servicios as es', 'es.id', 'reg.entidad_id')
                    ->join('users', 'users.id', 'es.user_id')
                    ->where('users.id', Auth::id())
                    ->select('reg.*')
                    ->get();
    }
}
