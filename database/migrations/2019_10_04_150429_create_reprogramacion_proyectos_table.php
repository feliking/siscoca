<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReprogramacionProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reprogramacion_proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('proyecto_id')->nullable();
            $table->foreign('proyecto_id')->references('id')
            ->on('proyectos')->onDelete('cascade');
            $table->unsignedInteger('solicitud_aprobada_id')->nullable();
            $table->foreign('solicitud_aprobada_id')->references('id')
            ->on('solicitud_aprobadas')->onDelete('cascade');
            $table->date('fecha_reprogramacion');
            $table->string('observaciones');
            $table->string('responsable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reprogramacion_proyectos');
    }
}
