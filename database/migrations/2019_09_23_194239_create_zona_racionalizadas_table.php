<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZonaRacionalizadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('racionalizacion_erradicacion_diaria', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_racionalizacion');
            $table->unsignedInteger('departamento_id');
            $table->unsignedInteger('regional_id');
            $table->unsignedInteger('unidad_id');
            $table->unsignedInteger('tipo_ejecucion_id');
            $table->double('hectareas_afectadas');
            $table->foreign('departamento_id')->references('id')->on('departamentos')->onDelete('cascade');
            $table->foreign('regional_id')->references('id')->on('regionales')->onDelete('cascade');
            $table->foreign('unidad_id')->references('id')->on('unidad_dependientes')->onDelete('cascade');
            $table->foreign('tipo_ejecucion_id')->references('id')->on('tipo_ejecuciones')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('racionalizacion_erradicacion_diaria');
    }
}
