<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogEventosSistemaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_eventos_sistema', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->string('usuario',100);
            $table->string('titulo',100);
            $table->string('descripcion',1500);
            $table->string('lista_adjunto')->nullable();
            $table->integer('prioridad_id');
            $table->string('estado',1)->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_eventos_sistema');
    }
}
