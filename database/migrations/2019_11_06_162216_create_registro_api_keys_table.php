<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistroApiKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_api_keys', function (Blueprint $table) {
            $table->increments('id');
            $table->text('apiKey');
            $table->datetime('fecha_finalizacion');
            $table->unsignedInteger('entidad_id');
            $table->foreign('entidad_id')->references('id')
            ->on('entidad_servicios');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_api_keys');
    }
}
