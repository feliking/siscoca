<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReclamosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reclamos', function (Blueprint $table) {
           $table->increments('id');
           $table->string('problema');
           $table->text('descripcion_problema');
           $table->string('codigo_reclamo');
           $table->date('fecha_nacimiento')->nullable();
           $table->string('carnet_identidad');   
           $table->string('email')->nullable();           
           $table->timestamps();
           $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reclamos');
    }
}
