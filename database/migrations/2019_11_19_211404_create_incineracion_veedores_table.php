<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncineracionVeedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incineracion_veedores', function (Blueprint $table) {
            $table->integer('incineracion_id')->unsigned();
            $table->foreign('incineracion_id')->references('id')->on('incineraciones');
            $table->integer('veedor_id')->unsigned();
            $table->foreign('veedor_id')->references('id')->on('veedores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incineracion_veedores');
    }
}
