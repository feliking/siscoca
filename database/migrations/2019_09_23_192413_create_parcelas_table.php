<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParcelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcelas', function (Blueprint $table) {
            $table->increments('id');
            $table->double('edad_parcela');
            $table->string('codigo')->nullable();
            $table->integer('comunidad_id')->unsigned()->nullable();
            $table->foreign('comunidad_id')->references('id')->on('comunidades');
            $table->string('latitud');
            $table->string('longitud');
            $table->string('descripcion', 500)->nullable();
            $table->double('hectareas');
            $table->integer('motivo_actualizacion_id')->unsigned()->nullable();
            $table->foreign('motivo_actualizacion_id')->references('id')->on('motivos_actualizacion');
            $table->string('codigo_catastral')->nullable();
            $table->integer('region_id')->unsigned()->nullable();
            $table->foreign('region_id')->references('id')->on('regionales');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('autorizacion_id')->unsigned()->nullable();;
            $table->foreign('autorizacion_id')->references('id')->on('autorizacion_renovaciones');
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('racionalizado')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcelas');
    }
}
