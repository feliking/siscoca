<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConclusionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conclusiones', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_conclusion');
            $table->double('presupuesto_ejecutado');
            $table->string('informe');
            $table->string('observaciones');
            $table->unsignedInteger('capacitacion_id')->nullable();
            $table->foreign('capacitacion_id')->references('id')->on('capacitaciones')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conclusiones');
    }
}
