<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluacionImpactosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluacion_impactos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_evaluacion');
            $table->string('informe');
            $table->jsonb('adjuntos');
            $table->string('observaciones');
            $table->unsignedInteger('capacitacion_id');
            $table->foreign('capacitacion_id')->references('id')->on('capacitaciones')
            ->onDelete('cascade');
            $table->timestamps();
        });
        Schema::table('capacitaciones', function (Blueprint $table){
            $table->unsignedInteger('conclusion_id')->nullable();
            $table->foreign('conclusion_id')->references('id')->on('conclusiones')
            ->ondelete('cascade');
            $table->unsignedInteger('evaluacion_id')->nullable();
            $table->foreign('evaluacion_id')->references('id')->on('evaluacion_impactos')
            ->ondelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluacion_impactos');
    }
}
