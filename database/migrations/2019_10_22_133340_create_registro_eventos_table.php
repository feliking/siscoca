<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistroEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('evento_id')->nullable();
            $table->string('observaciones')->nullable();
            $table->foreign('evento_id')->on('eventos_exepcionales')
            ->references('id')->onDelete('cascade');
            $table->jsonb('detalle')->nullable();
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->on('users')
            ->references('id')->onDelete('cascade');
            $table->datetime('hora_inicio')->nullable();
            $table->datetime('hora_fin')->nullable();
            $table->unsignedInteger('puesto_id')->nullable();
            $table->foreign('puesto_id')->on('puestos_de_control')
            ->references('id')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_eventos');
    }
}
