<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCancelacionProyectosTable extends Migration
{
    public function up()
    {
        Schema::create('cancelacion_proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('proyecto_id')->nullable();
            $table->foreign('proyecto_id')->references('id')
            ->on('proyectos')->onDelete('cascade');
            $table->unsignedInteger('solicitud_aprobada_id')->nullable();
            $table->foreign('solicitud_aprobada_id')->references('id')
            ->on('solicitud_aprobadas')->onDelete('cascade');
            $table->string('observaciones');
            $table->date('fecha_cancelacion');
            $table->string('responsable');
            $table->timestamps();
        });
        Schema::table('proyectos', function (Blueprint $table){
            $table->unsignedInteger('cancelacion_id')->nullable();
            $table->foreign('cancelacion_id')->references('id')
            ->on('cancelacion_proyectos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancelacion_proyectos');
    }
}
