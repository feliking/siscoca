<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyObrasProductorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proy_obras_productor', function (Blueprint $table) {
           $table->increments('id');
           $table->string('nombre');
           $table->text('descripcion')->nullable();
           $table->integer('tipo_proy_obras_productor_id')->unsigned();
		   $table->foreign('tipo_proy_obras_productor_id')->references('id')->on('tipo_proy_obras_productor');           
           $table->integer('persona_id')->unsigned();
           $table->foreign('persona_id')->references('id')->on('personas');            
           $table->timestamps();
           $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proy_obras_productor');
    }
}
