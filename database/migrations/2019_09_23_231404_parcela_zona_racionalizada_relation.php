<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ParcelaZonaRacionalizadaRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('racionalizacion_erradicacion_diaria', function(Blueprint $table) {
            $table->unsignedInteger('parcela_id')->nullable();
            $table->foreign('parcela_id')->references('id')->on('parcelas')->onDelete('cascade');

        });
        Schema::table('parcelas', function(Blueprint $table){
            $table->unsignedInteger('zona_racionalizada_id')->nullable();
            $table->foreign('zona_racionalizada_id')->references('id')->on('racionalizacion_erradicacion_diaria')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('racionalizacion_erradicacion_diaria');
        Schema::dropIfExists('parcelas');
    }
}
