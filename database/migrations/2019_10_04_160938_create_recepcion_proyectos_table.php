<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecepcionProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recepcion_proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('proyecto_id');
            $table->foreign('proyecto_id')->references('id')->on('proyectos')
            ->onDelete('cascade');
            $table->unsignedInteger('solicitud_aprobada_id');
            $table->foreign('solicitud_aprobada_id')->references('id')
            ->on('solicitud_aprobadas')->onDelete('cascade');
            $table->date('fecha_recepcion');
            $table->string('observaciones');
            $table->jsonb('lista_adjuntos');
            $table->timestamps();
        });
        Schema::table('proyectos', function(BluePrint $table){
            $table->unsignedInteger('recepcion_id')->nullable();
            $table->foreign('recepcion_id')->references('id')
            ->on('recepcion_proyectos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recepcion_proyectos');
    }
}
