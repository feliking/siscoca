<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->unsignedInteger('municipio_id')->nullable();
            $table->foreign('municipio_id')->references('id')
            ->on('municipios')->onDelete('cascade');
            $table->unsignedInteger('convocatoria_id')->nullable();
            $table->foreign('convocatoria_id')->references('id')
            ->on('convocatorias')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::table('solicitudes', function (Blueprint $table) {
            $table->integer('proyecto_id')->unsigned()->nullable();
            $table->foreign('proyecto_id')->references('id')->on('proyectos');
        });
        DB::table('convocatorias', function (Blueprint $table){
            $table->unsignedInteger('id')->nullable();
            $table->foreign('id')->references('id')->on('convocatorias')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
