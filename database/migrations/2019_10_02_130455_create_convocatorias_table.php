<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvocatoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocatorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_convocatoria_id')->unsigned();
            $table->foreign('tipo_convocatoria_id')->references('id')->on('tipo_convocatorias');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->string('terminos_referencia');
            $table->string('indicaciones_calificables');
            $table->string('codigo_referencia');
            $table->integer('tipo_proyecto'); // FONADIN = 1, OII = 2
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convocatorias');
    }
}
