<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapacitacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capacitaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_capacitacion');
            $table->string('tipo_actividad');
            $table->string('organizacion');
            $table->string('responsable_nombre');
            $table->double('presupuesto');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("ALTER TABLE capacitaciones ADD COLUMN searchtext TSVECTOR");
        DB::statement("UPDATE capacitaciones SET searchtext = to_tsvector('spanish', organizacion || '' || responsable_nombre || '' || tipo_actividad)");
        DB::statement("CREATE INDEX searchtext_gin ON capacitaciones USING GIN(searchtext)");
        DB::statement("CREATE TRIGGER ts_searchtext_capacitaciones BEFORE INSERT OR UPDATE ON capacitaciones FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('searchtext', 'pg_catalog.spanish', 'organizacion', 'responsable_nombre', 'tipo_actividad')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP TRIGGER IF EXISTS ts_searchtext_capacitaciones ON capacitaciones");
        DB::statement("DROP INDEX IF EXISTS searchtext_gin");
        DB::statement("ALTER TABLE capacitaciones DROP COLUMN searchtext");
        Schema::dropIfExists('capacitaciones');
    }
}
