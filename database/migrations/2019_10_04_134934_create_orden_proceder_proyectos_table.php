<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenProcederProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_proceder_proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_emision');
            $table->string('responsable_nombre');
            $table->string('observaciones');
            $table->unsignedInteger('solicitud_aprobada_id');
            $table->foreign('solicitud_aprobada_id')->references('id')
            ->on('solicitud_aprobadas')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::table('solicitud_aprobadas', function(Blueprint $table) {
            $table->unsignedInteger('orden_id')->nullable();
            $table->foreign('orden_id')->references('id')
            ->on('orden_proceder_proyectos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_proceder_proyectos');
    }
}
