<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeguimientoProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguimiento_proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('proyecto_id')->nullable();
            $table->foreign('proyecto_id')->references('id')
            ->on('proyectos')->onDelete('cascade');
            $table->date('fecha_visita');
            $table->double('porcentaje_avance');
            $table->jsonb('adjuntos');
            $table->string('observaciones');
            $table->string('ruta_form_ambiental');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seguimiento_proyectos');
    }
}
