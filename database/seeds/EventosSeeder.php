<?php

use Illuminate\Database\Seeder;

class EventosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['titulo'=> 'Omision de puesto de Control en Hoja de Ruta',
             'descripcion'=>'Un usuario omitio un puesto de control en su ruta',
             'prioridad'=>'ALTA',
             'fillable'=>false],
             ['titulo'=> 'Omision de registro guia de internacion',
             'descripcion'=>'Un usuario omitio un puesto de control en su hoja deruta',
             'prioridad'=>'ALTA',
             'fillable'=>false]
        ];
        foreach ($data as $data) {
			App\Models\Notificaciones\EventosExepcionales::create($data);
		}
    }
}
