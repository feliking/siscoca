<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class unidades_seeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_unidades')->insert([
            'descripcion'=>'Unidad de Erradicacion'
        ]);
        DB::table('unidad_dependientes')->insert([[
            'tipo_id'=>'1',
            'descripcion'=>'UDESY'
        ],[
            'tipo_id'=>'1',
            'descripcion'=>'UDESTRO'
        ]]);
    }
}
