<?php

use Illuminate\Database\Seeder;

class PersonasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\Persona::class, 200000)->create();
        factory(App\Persona::class, 10000)->create();
    }
}
