<?php

use Illuminate\Database\Seeder;

class ParcelaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parcelas')->insert([[
            'latitud'=>'-16.2779603063300',
            'longitud'=>'-62.290890122873',
            'hectareas'=> '500',
            'user_id'=>'1',
            'codigo_catastral'=> '6694833',
            'fecha_parcela'=>date('d-m-Y')
        ],[
            'latitud'=>'-16.2779603063300',
            'longitud'=>'-62.290890122873',
            'hectareas'=> '600',
            'user_id'=>'1',
            'codigo_catastral'=> '18232158',
            'fecha_parcela'=>date('d-m-Y')
        ],[
            'latitud'=>'-16.2779603063300',
            'longitud'=>'-62.290890122873',
            'hectareas'=> '900',
            'user_id'=>'1',
            'codigo_catastral'=> '7548193',
            'fecha_parcela'=>date('d-m-Y')
        ],[
            'latitud'=>'-16.2779603063300',
            'longitud'=>'-62.290890122873',
            'hectareas'=> '200',
            'user_id'=>'1',
            'codigo_catastral'=> '54802321',
            'fecha_parcela'=>date('d-m-Y')
        ],[
            'latitud'=>'-16.2779603063300',
            'longitud'=>'-62.290890122873',
            'hectareas'=> '100',
            'user_id'=>'1',
            'codigo_catastral'=> '69502432',
            'fecha_parcela'=>date('d-m-Y')
        ],[
            'latitud'=>'-16.2779603063300',
            'longitud'=>'-62.290890122873',
            'hectareas'=> '500',
            'user_id'=>'1',
            'codigo_catastral'=> '53297523',
            'fecha_parcela'=>date('d-m-Y')
        ],[
            'latitud'=>'-16.2779603063300',
            'longitud'=>'-62.290890122873',
            'hectareas'=> '670',
            'user_id'=>'1',
            'codigo_catastral'=> '58208932',
            'fecha_parcela'=>date('d-m-Y')
        ],[
            'latitud'=>'-16.2779603063300',
            'longitud'=>'-62.290890122873',
            'hectareas'=> '800',
            'user_id'=>'1',
            'codigo_catastral'=> '52439112',
            'fecha_parcela'=>date('d-m-Y')
        ],[
            'latitud'=>'-16.2779603063300',
            'longitud'=>'-62.290890122873',
            'hectareas'=> '300',
            'user_id'=>'1',
            'codigo_catastral'=> '09893421',
            'fecha_parcela'=>date('d-m-Y')
        ],[
            'latitud'=>'-16.2779603063300',
            'longitud'=>'-62.290890122873',
            'hectareas'=> '900',
            'user_id'=>'1',
            'codigo_catastral'=> '473819384',
            'fecha_parcela'=>date('d-m-Y')
        ]]);
    }
}
