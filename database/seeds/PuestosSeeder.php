<?php

use Illuminate\Database\Seeder;
use App\PuestoControl;

class PuestosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'YUCUMO',
            'descripcion' => 'Puesto de control',
            'latitud' => -15.35209367916587,
            'longitud' => -66.7307299411982,
            'direccion' => 'Beni'
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'TRINIDAD',
            'descripcion' => 'Puesto de control',
            'latitud' => -14.909695893577148,
            'longitud' => -64.8850268161982,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'RIBERALTA',
            'descripcion' => 'Puesto de control',
            'latitud' => -11.05652765418447,
            'longitud' => -66.0276049411982,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'SUCRE',
            'descripcion' => 'Puesto de control',
            'latitud' => -19.007347539704806,
            'longitud' => -65.2756518397947,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'SUTICOLLO',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.59478503753576,
            'longitud' => -66.3523119960447,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'CASTILLO',
            'descripcion' => 'Puesto de control',
            'latitud' => -16.71600225727896,
            'longitud' => -65.3855151210447,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'LOCOTAL',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.017895406598022,
            'longitud' => -65.6382006679197,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'MERCADO DE SACABA',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.346270287267355,
            'longitud' => -65.9568041835447,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'CAMPAMENTO CENTRAL AGRIGENTO',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.013393139372408,
            'longitud' => -65.0009936366697,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'BULO BULO',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.241373628348416,
            'longitud' => -64.3857592616697,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'CAMPAMENTO MAS CERCANO',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.07341506905012,
            'longitud' => -64.7483080897947,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'LA RINCONADA',
            'descripcion' => 'Puesto de control',
            'latitud' => -16.432705390303273,
            'longitud' => -68.06611059447198,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'ACHICA ARRIBA',
            'descripcion' => 'Puesto de control',
            'latitud' => -16.77261850854718,
            'longitud' => -68.15400121947198,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'BAJO CHORO',
            'descripcion' => 'Puesto de control',
            'latitud' => -16.06059199375514,
            'longitud' => -67.71184398805451,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'APOLO',
            'descripcion' => 'Puesto de control',
            'latitud' => -14.72132674317159,
            'longitud' => -68.37170430102054,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'CHARAZANI',
            'descripcion' => 'Puesto de control',
            'latitud' => -15.332193685993415,
            'longitud' => -69.04187031664554,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'CAMPAMENTO CARANAVI',
            'descripcion' => 'Puesto de control',
            'latitud' => -16.15244518050266,
            'longitud' => -67.27307148852054,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'CAMPAMENTO INCAHUARA LA ASUNTA',
            'descripcion' => 'Puesto de control',
            'latitud' => -16.093643128021125,
            'longitud' => -67.51477070727054,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'DIRECCION DEPARTAMENTAL ORURO',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.99781779662019,
            'longitud' => -67.0641481130109,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'INQUISIVI',
            'descripcion' => 'Puesto de control',
            'latitud' => -16.79219779463698,
            'longitud' => -67.1959840505109,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'CHALLAPATA',
            'descripcion' => 'Puesto de control',
            'latitud' => -18.890375858671742,
            'longitud' => -66.7730104176984,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'COBIJA',
            'descripcion' => 'Puesto de control',
            'latitud' => -11.060326214224967,
            'longitud' => -68.74474320397451,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'POTOSI',
            'descripcion' => 'Puesto de control',
            'latitud' => -19.561224939147696,
            'longitud' => -65.75254418560121,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'DIRECCION DEPARTAMENTAL SANTA CRUZ',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.75627741776405,
            'longitud' => -63.15961699765285,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'KM40',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.398687318609124,
            'longitud' => -64.02753691952785,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'MONTERO',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.34176708157874,
            'longitud' => -63.34638457577785,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'LOS TRONCOS',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.032893468264465,
            'longitud' => -62.64325957577785,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'PAILON',
            'descripcion' => 'Puesto de control',
            'latitud' => -17.68302162546419,
            'longitud' => -62.55536895077785,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'TARIJA',
            'descripcion' => 'Puesto de control',
            'latitud' => -21.4824694116092,
            'longitud' => -64.76246088355941,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'REGIONAL YACUIBA',
            'descripcion' => 'Puesto de control',
            'latitud' => -21.937410672952993,
            'longitud' => -63.64185541480941,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'REGIONAL VILLAMONTES',
            'descripcion' => 'Puesto de control',
            'latitud' => -21.257387882119094,
            'longitud' => -63.35621088355941,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'REGIONAL BERMEJO',
            'descripcion' => 'Puesto de control',
            'latitud' => -22.557683266438165,
            'longitud' => -64.32300775855941,
            'direccion' => 'Beni' 
        ]);
        PuestoControl::create([
            'municipio_id' => 1,
            'codigo' => 'CODE',
            'nombre' => 'REGIONAL VILLAZON',
            'descripcion' => 'Puesto de control',
            'latitud' => -21.816525248537438,
            'longitud' => -65.48755853980941,
            'direccion' => 'Beni' 
        ]);
    }
}
