<?php

use Illuminate\Database\Seeder;

class TiposPrioridadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
			['id' => '1', 'nombre' => 'Baja'],
            ['id' => '2', 'nombre' => 'Media'],
            ['id' => '3', 'nombre' => 'Alta'],
		];	foreach ($data as $data) {
			App\TipoPrioridad::create($data);
		}
    }
}
