<?php

use Illuminate\Database\Seeder;

class TipoEjecuciondata extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_ejecuciones')->insert([[
            'descripcion_ejecucion'=>'Erradicacion'
        ],[
            'descripcion_ejecucion'=>'Racionalizacion'
        ]]);
    }
}
