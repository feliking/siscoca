<?php

use Faker\Generator as Faker;

$factory->define(App\Persona::class, function (Faker $faker) {
    return [
        'tipo_persona_id' => 3,
        'nombre' => $faker->name,
        'primer_apellido' => $faker->name,
        'segundo_apellido' => $faker->name,
        'carnet_identidad' => str_random(6),
        'departamento_extension_id' => 1,
        'estado_civil' => 'Soltero',
        'genero' => 'M',
        'telefono' => '123456',
        'email' => $faker->safeEmail,
        'domicilio' => 'Algun domicilio',
        'fecha_nacimiento' => '2019-09-27',
        'pais_nacimiento_id' => 1,
        'departamento_nacimiento_id' => 1,
        'provincia_nacimiento_id' => 1,
        'fotografia' => 'App/kernel',
        'biometrico' => '123456789',
        'carnet_escaneado' => 'App/asss',
        'activo' => true,
        'user_id' => 1
    ];
});
