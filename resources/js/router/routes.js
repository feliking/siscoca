export default [
  ...applyRules(['guest'], [
    { path: '', component: require('$comp/auth/AuthWrapper'), redirect: { name: 'home' }, children:
      [
        { path: '/home', name: 'home', component: require('$comp/auth/home/Index'), meta: {rule: 'public'} },
        { path: '/login', name: 'login', component: require('$comp/auth/login/Login'), meta: {rule: 'public'} },
        { path: '/consulta_registro', name: 'consulta_registro', component: require('$comp/auth/home/consulta_registro/Index'), meta: {rule: 'public'} },
      ]
    },
  ]),
  ...applyRules(['auth'], [
    { path: '', component: require('$comp/admin/AdminWrapper'), children:
      [
        { path: '', name: 'index', redirect: { name: 'profile' } },
        { path: 'profile', component: require('$comp/admin/profile/ProfileWrapper'), meta: {rule: 'public'}, children:
          [
            { path: '', name: 'profile', component: require('$comp/admin/profile/Profile'), meta: {rule: 'public'} },
            { path: 'edit', name: 'profile-edit', component: require('$comp/admin/profile/edit/ProfileEdit'), meta: {rule: 'public'} }
          ]
        },
        /* 1 Modulo de informativo */
        { path: 'normativas', name: 'normativa', component: require('$comp/portal_informativo/normativa/Index'), meta: {rule: 'admin'} },
        { path: 'proyecto_obras', name: 'proyecto_obra', component: require('$comp/portal_informativo/proyecto_obra/Index'), meta: {rule: 'admin'} },
        { path: 'prod_autorizaciones', name: 'prod_autorizacion', component: require('$comp/portal_informativo/prod_autorizacion/Index'), meta: {rule: 'admin'} },
        { path: 'prod_industrializados', name: 'prod_industrializado', component: require('$comp/portal_informativo/prod_industrializado/Index'), meta: {rule: 'admin'} },
        { path: 'investigaciones_coca', name: 'investigacion_coca', component: require('$comp/portal_informativo/investigacion_coca/Index'), meta: {rule: 'admin'} },
        { path: 'enlaces_institucionales', name: 'enlace_institucional', component: require('$comp/portal_informativo/enlace_institucional/Index'), meta: {rule: 'admin'} },
        { path: 'consultas_portal_info', name: 'consulta_portal_info', component: require('$comp/portal_informativo/consulta_portal_info/Index'), meta: {rule: 'admin'} },
        { path: 'reclamos_portal_info', name: 'reclamo_portal_info', component: require('$comp/portal_informativo/reclamo_portal_info/Index'), meta: {rule: 'admin'} },
        { path: 'callcenters', name: 'callcenter', component: require('$comp/portal_informativo/callcenter/Index'), meta: {rule: 'admin'} },         
        
        /* 2 Modulo de registro */
        { path: 'productores', name: 'productor', component: require('$comp/admin/productor/Index'), meta: {rule: 'digprococa'} },
        { path: 'productores-al-detalle', name: 'productorDetalle', component: require('$comp/admin/productor_detalle/Index'), meta: {rule: 'digcoin'} },
        { path: 'comercializadores', name: 'comercializador', component: require('$comp/admin/comercializador/Index'), meta: {rule: 'digcoin'} },
        { path: 'empresas', name: 'empresa', component: require('$comp/admin/empresa/Index'), meta: {rule: 'digcoin'} },
        { path: 'entidad-de-investigacion', name: 'entidad_investigacion', component: require('$comp/admin/entidad_investigacion/Index'), meta: {rule: 'digcoin'} },
        { path: 'beneficiarios-de-donaciones', name: 'beneficiario_donacion', component: require('$comp/admin/beneficiario_donacion/Index'), meta: {rule: 'digcoin'} },
        { path: 'beneficiarios-de-desarrollo', name: 'beneficiario_desarrollo', component: require('$comp/admin/beneficiario_desarrollo/Index'), meta: {rule: 'digcoin'} },
        
        

        /* 3 Modulo de actividades de produccion */

        { path: 'caracteristicas_produccion', name: 'caracteristica_produccion', component: require('$comp/admin/caracteristica_produccion/Index'), meta: {rule: 'digcoin'} },
        { path: 'autorizacion_renov_parcela', name: 'autorizacion_renov_parcela', component: require('$comp/admin/autorizacion_renov_parcela/Index'), meta: {rule: 'digcoin'} },
        { path: 'consulta_ren_ver', name: 'consulta_ren_ver', component: require('$comp/admin/consulta_ren_ver/Index'), meta: {rule: 'digcoin'} },
        { path: 'zonas_racionalizadas', name:'zonas_racionalizadas', component: require('$comp/admin/zona_racionalizada/Index'), meta:{rule: 'admin'}},

        /* 4 Modulo de actividades de comercializacion*/

        { path: 'guias_internacion', name: 'guia_internacion', component: require('$comp/admin/guia_internacion/Index'), meta: {rule: 'digcoin'} },
        { path: 'control_guias_internacion', name: 'control_guia_internacion', component: require('$comp/admin/control_guia_internacion/Index'), meta: {rule: 'digcoin'} },
        { path: 'areas_puestos_venta', name: 'puesto_venta', component: require('$comp/admin/puesto_venta/Index'), meta: {rule: 'digcoin'} },        
        { path: 'retenciones', name: 'retencion', component: require('$comp/admin/retencion/Index'), meta: {rule: 'digcoin'} },
        { path: 'decomisos', name: 'decomiso', component: require('$comp/admin/decomiso/Index'), meta: {rule: 'digcoin'} },
        { path: 'donaciones', name: 'donacion', component: require('$comp/admin/donacion/Index'), meta: {rule: 'digcoin'} },
        { path: 'incineraciones', name: 'incineracion', component: require('$comp/admin/incineracion/Index'), meta: {rule: 'digcoin'} },
        { path: 'devoluciones', name: 'devolucion', component: require('$comp/admin/devolucion/Index'), meta: {rule: 'digcoin'} },
        { path: 'depositos', name: 'deposito', component: require('$comp/admin/deposito/Index'), meta: {rule: 'digcoin'} },
        { path: 'roles', name: 'roles', component: require('$comp/admin/roles/Role'), meta: {rule: 'admin'} },
        { path: 'permisos', name: 'permission', component: require('$comp/admin/permission/Permission'), meta: {rule: 'admin'} },
        { path: 'usuarios', name: 'user', component: require('$comp/admin/user/User'), meta: {rule: 'admin'} },        
        { path: 'poderes', name: 'poder', component: require('$comp/admin/poder/Index'), meta: {rule: 'digcoin'} },
        { path: 'hojas_de_ruta', name: 'hoja_ruta', component: require('$comp/admin/hoja_ruta/Index'), meta: {rule: 'digcoin'} },
        { path: 'hojas_ruta_trueque', name: 'hoja_ruta_trueque', component: require('$comp/admin/hoja_ruta_trueque/Index'), meta: {rule: 'digcoin'} },
        { path: 'control_hojas_de_ruta', name: 'control_hoja_ruta', component: require('$comp/admin/control_hoja_ruta/Index'), meta: {rule: 'digcoin'} },
        
        /* 5 Modulo de desarrollo integral */
        // { path: 'proyecto_convocatoria', name: 'proyecto_convocatoria', component: require('$comp/desarrollo_integral/proyecto_convocatoria/Index'), meta: {rule: 'fonadin'} },
        // { path: 'proyecto_solicitud', name: 'proyecto_solicitud', component: require('$comp/desarrollo_integral/proyecto_solicitud/Index'), meta: {rule: 'fonadin'} },
        // { path: 'apertura_sobres', name: 'apertura_sobres', component: require('$comp/desarrollo_integral/apertura_sobres/Index'), meta: {rule: 'fonadin'} },
        // { path: 'proyecto_evaluacion', name: 'proyecto_evaluacion', component: require('$comp/desarrollo_integral/proyecto_evaluacion/Index'), meta: {rule: 'fonadin'} },
        // { path: 'proyecto_cofinanciamiento', name: 'proyecto_cofinanciamiento', component: require('$comp/desarrollo_integral/proyecto_cofinanciamiento/Index'), meta: {rule: 'fonadin'} },
        // { path: 'proyecto_ejecucion', name: 'proyecto_ejecucion', component: require('$comp/desarrollo_integral/proyecto_ejecucion/Index'), meta: {rule: 'fonadin'} },
        
        { path: 'convocatorias_oii', name: 'convocatorias_oii', component: require('$comp/desarrollo_integral/convocatorias_oii/Index'), meta: {rule: 'oii'} },
        { path: 'proyectos', name: 'proyectos', component: require('$comp/desarrollo_integral/proyectos/Index'), meta: {rule:'oii'}},
        { path: 'recepcion_solicitud_oii', name: 'recepcion_solicitud_oii', component: require('$comp/desarrollo_integral/recepcion_solicitud_oii/Index'), meta: {rule: 'oii'} },
        { path: 'priorizacion_oii', name: 'priorizacion_oii', component: require('$comp/desarrollo_integral/priorizacion_oii/Index'), meta: {rule: 'oii'} },
        { path: 'evaluacion_viabilidad_oii', name: 'evaluacion_viabilidad_oii', component: require('$comp/desarrollo_integral/evaluacion_viabilidad_oii/Index'), meta: {rule: 'oii'} },
        { path: 'supervision_oii', name: 'supervision_oii', component: require('$comp/desarrollo_integral/supervision_oii/Index'), meta: {rule: 'oii'} },
        { path: 'cierre_obra_oii', name: 'cierre_obra_oii', component: require('$comp/desarrollo_integral/cierre_obra_oii/Index'), meta: {rule: 'oii'} },
        { path: 'evaluacion_impacto_oii', name: 'evaluacion_impacto_oii', component: require('$comp/desarrollo_integral/evaluacion_impacto_oii/Index'), meta: {rule: 'oii'} },


        // { path: 'capacitaciones', name: 'capacitaciones', component: require('$comp/desarrollo_integral/capacitaciones/Index'), meta: {rule: 'admin'}},
        // { path: 'proyecto_procesos', name: 'proyecto_procesos', component: require('$comp/desarrollo_integral/proyecto_procesos/Index'), meta: {rule:'fonadin'}},
        // { path: 'proyectos', name: 'proyectos', component: require('$comp/desarrollo_integral/proyectos/Index'), meta: {rule:'fonadin'}},
        

        /* 6 Modulo de resoluciones */
        { path: 'resoluciones_administrativas', name: 'resolucion_administrativa', component: require('$comp/admin/resolucion_administrativa/Index'), meta: {rule: 'digcoin'} },
        { path: 'consulta_res_administrativa', name: 'consulta_res_administrativa', component: require('$comp/admin/consulta_res_administrativa/Index'), meta: {rule: 'digcoin'} },
        { path: 'sanciones_monetarias', name: 'sanciones_monetarias', component: require('$comp/admin/sanciones_monetarias/Index'), meta: {rule: 'digcoin'} },
        { path: 'consulta_infracciones_san', name: 'consulta_infracciones_san', component: require('$comp/admin/consulta_infracciones_san/Index'), meta: {rule: 'digcoin'} },
        
        
        /* 7 Modulo de reportes */
        { path: "reportes_comercializacion", name: "reportes_comercializacion", component: require("$comp/admin/reportes_comercializacion/Index"), meta: { rule: "admin" } },
        { path: "reportes_industrializacion", name: "reportes_industrializacion", component: require("$comp/admin/reportes_industrializacion/Index"), meta: { rule: "admin" } },        
        
        /* 8 Modulo de alertas y notificaciones */
        {path: 'notificaciones', name: 'notificaciones', component: require('$comp/notificaciones/Index'), meta: {rule: 'admin'}},
        {path: 'notificaciones/nueva', name: 'notificaciones_generate', component: require('$comp/notificaciones/registro_eventos/Index'), meta:{rule: 'admin'}},
        {path: 'notificaciones/monitoreo', name: 'notificaciones_monitoreo', component: require('$comp/notificaciones/monitoreo_eventos/Index'), meta: {rule: 'admin'}},
        /* 9 Modulo de Usuarios y roles */

        /* 10 Modulo de parametrizado */
        { path: 'zona_autorizada', name: 'zona_autorizada', component: require('$comp/admin/zona_autorizada/Index'), meta: {rule: 'admin'} },
        { path: 'departamentos', name: 'departamento', component: require('$comp/admin/departamento/Index'), meta: {rule: 'admin'} },
        { path: 'provincias', name: 'provincia', component: require('$comp/admin/provincia/Index'), meta: {rule: 'admin'} },
        { path: 'municipios', name: 'municipio', component: require('$comp/admin/municipio/Index'), meta: {rule: 'admin'} },
        { path: 'federaciones', name: 'federacion', component: require('$comp/admin/federacion/Index'), meta: {rule: 'admin'} },
        { path: 'regionales', name: 'regional', component: require('$comp/admin/regional/Index'), meta: {rule: 'admin'} },
        { path: 'centrales', name: 'central', component: require('$comp/admin/central/Index'), meta: {rule: 'admin'} },
        { path: 'sindicatos', name: 'sindicato', component: require('$comp/admin/sindicato/Index'), meta: {rule: 'admin'} },
        { path: 'infracciones', name: 'infraccion', component: require('$comp/admin/infraccion/Index'), meta: {rule: 'admin'} },
        { path: 'sanciones', name: 'sancion', component: require('$comp/admin/sancion/Index'), meta: {rule: 'admin'} },
        { path: 'mercados', name: 'mercado', component: require('$comp/admin/mercado/Index'), meta: {rule: 'admin'} },
        { path: 'puestos_de_control', name: 'puesto_de_control', component: require('$comp/admin/puesto_de_control/Index'), meta: {rule: 'admin'} },
        { path: 'rutas', name: 'ruta', component: require('$comp/admin/ruta/Index'), meta: {rule: 'admin'} },
        { path: 'montos', name: 'monto', component: require('$comp/admin/monto/Index'), meta: {rule: 'admin'} },
        { path: 'localidades', name: 'localidad', component: require('$comp/admin/localidad/Index'), meta: {rule: 'admin'} },
        { path: 'marcas_vehiculos', name: 'marca', component: require('$comp/admin/marca/Index'), meta: {rule: 'admin'} },
        { path: 'vehiculos', name: 'vehiculo', component: require('$comp/admin/vehiculo/Index'), meta: {rule: 'admin'} },
        { path: 'tipo_convocatoria', name: 'tipo_convocatoria', component: require('$comp/desarrollo_integral/tipo_convocatoria/Index'), meta: {rule: 'oii'} },
        { path: 'firmas', name: 'firmas', component: require('$comp/admin/firmas/Index'), meta: {rule:'admin'} },
        { path: 'comunidades', name: 'comunidad', component: require('$comp/admin/comunidad/Index'), meta: {rule: 'admin'} },


        /* 11 Modulo de auditoria */
        { path: 'lista_eventos', name: 'lista_eventos', component: require('$comp/auditoria/lista_eventos/Index'), meta: {rule: 'admin'} },
        { path: 'registro_eventos', name: 'registro_eventos', component: require('$comp/auditoria/registro_eventos/Index'), meta: {rule: 'admin'} },

        /* 12 Modulo de servicios */
        { path: 'exposición_servicios', name: 'servicio', component: require('$comp/servicio/entidad_servicio/Index'), meta: {rule: 'admin'} },
        { path: 'generar_key', name: 'generar_key', component: require('$comp/servicio/generar_key/Index'), meta: {rule: 'admin'} }

        /* -- varios-- */
      ]
    },
  ]),
  { path: '*', redirect: { name: 'index' } }
]

function applyRules(rules, routes) {
  for (let i in routes) {
    routes[i].meta = routes[i].meta || {}

    if (!routes[i].meta.rules) {
      routes[i].meta.rules = []
    }
    routes[i].meta.rules.unshift(...rules)

    if (routes[i].children) {
      routes[i].children = applyRules(rules, routes[i].children)
    }
  }

  return routes
}
