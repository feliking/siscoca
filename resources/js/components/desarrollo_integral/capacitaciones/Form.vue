<template>
  <v-dialog persistent v-model="dialog" max-width="900px" @keydown.esc="close">
    <v-tooltip slot="activator" top>
      <v-icon large slot="activator" dark color="primary">add_circle</v-icon>
      <span>Nuevo</span>
    </v-tooltip>
    <v-card>
      <v-toolbar dark color="primary">
        <v-toolbar-title class="white--text">{{ formTitle }}</v-toolbar-title>
      </v-toolbar>
      <v-card-text>        
        <v-form ref="form" v-model="valid" lazy-validation>
          <v-layout wrap>
            <v-flex xs12 sm6 md6>
              <v-card-text>
                <v-menu
                        v-model="menu"
                        :close-on-content-click="false"
                        :nudge-right="40"
                        lazy
                        transition="scale-transition"
                        offset-y
                        full-width
                        max-width="290px"
                        min-width="290px"
                      >    
                <v-text-field
                          v-model="selectedItem.fecha_capacitacion"
                          label="Fecha de Capacitacion"
                          prepend-icon="event"
                          readonly
                          slot="activator"
                          :rules="[v => !!v || 'Requerido']"
                          hint="<span class='blue--text'>*Requerido</span>" persistent-hint
                          class="mr-2"
                        ></v-text-field>
                <v-date-picker v-model="selectedItem.fecha_capacitacion" no-title @input="menu = false"></v-date-picker>
                </v-menu>
                <v-text-field
                  v-model="selectedItem.tipo_actividad"
                  label="Tipo de Actividad"
                  :rules="[v => !!v || 'Requerido']"
                  hint="<span class='blue--text'>*Requerido</span>" persistent-hint
                  :error-messages="error"
                ></v-text-field>
                <v-text-field
                  v-model="selectedItem.organizacion"
                  label="Organizacion"
                  :rules="[v => !!v || 'Requerido', v => (v && v.length <= 50) || 'No mayor a 50 caracteres']"
                  hint="<span class='blue--text'>*Requerido</span>" persistent-hint
                ></v-text-field>
              </v-card-text>
            </v-flex>
            <v-spacer></v-spacer>
            <v-flex xs12 sm6 md6>
              <v-card-text>                
                <v-text-field
                  v-model="selectedItem.responsable_nombre"
                  label="Responsable"
                  :rules="[v => !!v || 'Requerido', v => (v && v.length <= 50) || 'No mayor a 50 caracteres']"
                  hint="<span class='blue--text'>*Requerido</span>" persistent-hint
                ></v-text-field>
                <v-text-field
                  v-model="selectedItem.presupuesto"
                  label="Presupuesto"
                  type="number"
                  :rules="[v => !!v || 'Requerido', v => (v && v.length <= 50) || 'No mayor a 50 caracteres']"
                  hint="<span class='blue--text'>*Requerido</span>" persistent-hint
                ></v-text-field>
                <v-autocomplete
                v-model="selectedItem.proyecto_id"
                :loading="loading"
                :items="proyectos"
                :search-input.sync="search"
                item-text="nombre"
                item-value="id"
                >
                </v-autocomplete>             
              </v-card-text>
            </v-flex>
          </v-layout>
        </v-form>
      </v-card-text>  
      <v-card-actions>
        <v-spacer></v-spacer>
        <v-btn color="error" small @click.native="close"><v-icon>close</v-icon> Cancelar</v-btn>
        <v-btn color="success" small :disabled="!valid" @click="save()" ><v-icon>check</v-icon> Guardar</v-btn>
      </v-card-actions>
    </v-card>
  </v-dialog>
</template>

<script>
import Vue from 'vue'
import axios from 'axios'

export default {
  props: ["item", "bus"],  
  data() {
    return {
      loading: false,
      search: null,      
      valid: true,
      menu: null,
      dialog: false,
      selectedIndex: -1,     
      selectedItem: {},
      error: '',
      proyectos: [],
      cancelSouce: null
    };
  },
  watch:{
    search (val){
      val && val !==this.select && this.getProyectos(val)
    }
  },
  created() {},  
  mounted() {
    this.bus.$on("openDialog", item => {
      this.selectedItem = item;
      this.dialog = true;
      this.selectedIndex = item;      
    });
  },
  computed: {
    formTitle() {
      return this.selectedIndex === -1 ? 'Nuevo ' : 'Editar '
    }    
  },
  methods: {
    close() {
      this.dialog = false;
      this.$refs.form.reset()
      this.bus.$emit("closeDialog");
      this.selectedIndex = -1;
      this.selectedItem = {}
    },
    getProyectos(val){
      if(val.length<3)
        return;
      this.cancelSearch();
      this.cancelSouce = axios.CancelToken.source();
      axios.get("api/proyectos/search?arg="+val,{
        cancelToken: this.cancelSouce.token  
      })
      .then(res=>{
        this.proyectos = res.data;
      })
    
    },cancelSearch(){
      if ( this.cancelSource) {
        this.cancelSouce.cancel('Start new search')
      }
    },
    async save() {
      try {
        if (this.$refs.form.validate()) {
          if (this.selectedIndex != -1) {
            await axios.put("api/capacitaciones/"+this.selectedItem.id, this.selectedItem)
          } else {
            await axios.post("api/capacitaciones", this.selectedItem)
          }
          this.$toast.success('Correcto.')
          this.close();
        }
      } catch(e) {
        console.log(e)
      }
    }
  },  
};
</script>