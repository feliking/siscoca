import Vue from 'vue'
import axios from 'axios'
import * as lmap from '../../admin/shared/leaf-map'

export default {
  props: ["item", "bus"], 
  components: {
    lmap
  },
  data() {
    return { 
      loading: false,
      bus_map: new Vue(),     
      valid: true,
      dialog: false,
      search:null,
      errorCodigo:null,
      selectedIndex: -1,     
      selectedItem: {
        latitud: null,
        longitud:null
      },
      departamentos:[],
      provincias:[],
      regionales:[],
      unidades:[],
      menu: null,
      place: null,
      error: '',
      statusText: '',
      parcelas:[],
      productores: [],
      tipo_ejecucion:[],
      cancelSource: null,
      racionalizacion_date: null,
      racionalizacion_date_formatted: null,
    };
  },
  watch:{
    search (val) {
      val && val !== this.select && this.getProductor(val)
    },
    racionalizacion_date(val){
      this.selectedItem.fecha_racionalizacion = val
      this.racionalizacion_date_formatted = this.$moment(val).format("DD/MM/YYYY") || this.$moment().format("DD/MM/YYYY");
    },     
  },
  created() {},  
  mounted() {
    this.bus.$on("openDialog", item => {
      console.log('editar zona');
      console.log(item);
      this.selectedItem = item;
      this.dialog = true;
      this.selectedIndex = item;
      this.bus_map.$emit("setMarker",{
        lat: item.latitud,
        lng: item.longitud
      })
    });
    this.bus_map.$on("marked", LatLng=>{
      this.selectedItem.latitud = LatLng.lat;
      this.selectedItem.longitud = LatLng.lng;
    })
    this.bus_map.$on("marker_id", item=>{
      let parcela = this.parcelas_productor.find((elem)=>{
        return elem.id=item.id;
      })
      this.selectedItem.longitud = parcela.longitud;
      this.selectedItem.latitud = parcela.latitud;
      this.selectedItem.hectareas = parcela.hectareas;
      this.selectedItem.parcela_id = parcela.id;
    });
    this.getEntryData();
    this.getTipoEjecuciones();
    //this.getParcelas();
  },
  computed: {
    formTitle() {
      return this.selectedIndex === -1 ? 'Nuevo ' : 'Editar '
    }    
  }, 
  methods: {
    openMap() {
      this.bus_map.$emit('openMap');
    },
    async getEntryData() {
      let response = await axios.get("api/departamento");
      this.departamentos = response.data;
      let unidad_res = await axios.get("api/unidades");
      this.unidades = unidad_res.data;
      let regionales_res = await axios.get("api/regional");
        this.regionales = regionales_res.data;
    },
    async getProvincias() {
        let response = await axios.get("api/provincia/fill/"+JSON.stringify({
            'departamento_id': this.selectedItem.departamento_id
        }));
        this.provincias = response.data;
    },
    getProductor(val){
      if(val.length<3)
        return;
      this.cancelSearch();
      this.cancelSource = axios.CancelToken.source();
      axios.get("api/productor?carnet="+val,{
        cancelToken: this.cancelSource.token})
        .then(res=>{
          this.productores = res.data;
        })
      this.errorCodigo = "no se encontro el codigo"
    },cancelSearch (){
      if (this.cancelSource) {
        this.cancelSource.cancel('Start new search, stop active search');
      }
    },
    async getParcelasProductor(id){
      let response = await axios.get("api/parcela/productor/"+id);
      this.parcelas_productor = response.data;
      this.bus_map.$emit("clearMap");
      this.parcelas_productor.forEach(elem=>{
        this.bus_map.$emit("addExtraMarker", {
          latlng: [elem.latitud, elem.longitud],
          id: elem.id,
        })
      });
    },
    getParcela(){
      let parcela = this.parcelas.find(elem=>{
        return elem.id == this.selectedItem.parcela_id
      });
      this.bus_map.$emit("setMarker",{
        lat: parcela.latitud,
        lng: parcela.longitud
      })
      this.selectedItem.longitud = parcela.longitud;
      this.selectedItem.latitud = parcela.latitud;
      this.selectedItem.hectareas = parcela.hectareas;
    },
    async getTipoEjecuciones(){
      let response = await axios.get("api/ejecucion/");
      this.tipo_ejecucion = response.data;
    },
    close() {
      this.dialog = false;
      this.$refs.form.reset()
      this.bus.$emit("closeDialog");
      this.selectedIndex = -1;
      this.selectedItem = {
        puestos_de_control:[]
      };
      this.markersTemp = [];
    },
    async save() {
      try {
        if (this.$refs.form.validate()) {
            await axios.post("api/zonas_racionalizadas", this.selectedItem)
          this.$toast.success('Correcto.')
          this.close();
        }
      } catch(e) {
        console.log(e)
      }
    },
    setDescription(description) {
      this.description = description;
    },
  }
}