<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="border: 0;border-radius: 0;">
    <div>
        <table width="100%">
            <tr>
                <td>
                <img src="{{ public_path("/img/escudo.png") }}" width="90">
                </td>
                <td style="text-align: center;font-size: 9pt;line-height:20px;">
                MINISTERIO DE DESARROLLO RURAL Y TIERRAS<br>
                VICEMINISTERIO DE COCA Y DESARROLLO INTEGRAL<br>
                DIRECCION GRAL. DE LA HOJA DE COCA E INDUSTRIALIZACIÓN
                </td>
                <td style="text-align:right;">
                <img src="{{ public_path("/img/mdryt.png") }}" width="160">
                </td>                
            </tr>
        </table>
    </div>
</body>
</html>