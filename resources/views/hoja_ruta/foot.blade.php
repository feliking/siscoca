<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="border: 0;border-radius: 0;">
    <hr>
    <div>
        <table width="100%">
            <tr>
                <td class="text-center" style="height: 100px; font-size: 19px" align="justify">
                    <b>NOTA:</b> La presente HOJA DE RUTA, autoriza a quién le sea extendido legalmente, al traslado de la Hoja
                    de Coca de los centros de producción a los mercados campesinos y de estos a los compradores y/o
                    detallistas. En caso de que el presente Comprobante tenga borrones o alteraciones quedará nulo en
                    su valor legal.
                </td>               
            </tr>
        </table>
    </div>
</body>
</html>