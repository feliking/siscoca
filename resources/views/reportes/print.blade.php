<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        <?php include public_path('css/report.css') ?>
    </style>
</head>

<body style="border: 0;border-radius: 0;">
    <table class="w-100 ">
        <tr>
            <th class="w-75 text-left no-padding no-margins align-middle">
                <div class="text-center">
                    {{ $title}}
                </div>
            </th>
            <th class="w-25 align-top">
                <span class="font-semibold leading-tight text-md">
                    FECHA: {{$fecha}}
                </span>
            </th>
        </tr>
    </table>
    <div class="page-break p-10" style="border-radius: 0.75em;border: 1px solid #22292f;">
        <div class="block">

            <table class="w-100 m-b-10">
                <thead>
                    <tr class="trborde">
                        @for ($i = 0; $i < count($heading); ++$i) <th class="trborde">{{$heading[$i]}}</th>
                            @endfor

                    </tr>
                </thead>
                <tbody class="">
                    @for ($i = 0; $i < count($report); ++$i) <tr class="trborde">
                        @for ($j=0; $j < count($keys); ++$j)<td align="center" class="borde">
                            {{$report[$i][$keys[$j]]}}
                            </td>

                            @endfor
                            </tr>
                            @endfor

                </tbody>
            </table>

        </div>
    </div>
    <div class="text-xxxs" align="right"> {{ 'Impreso el '.date('d/m/Y H:i') }} </div>
    <style>
        .borde {
            border-right: thin dotted;
            /* border-left: thin dotted; */
            border-bottom: thin dotted;
            border-width: 1px;
        }

        .trborde {
            border-bottom: thin dotted;
            border-width: 1px;
        }
    </style>
</body>

</html>
