<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> 
    <style type="text/css" media="print">
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
        .crop {
            position:absolute;
            left: -100%;
            right: -100%;
            top: -100%;
            bottom: -100%;
            margin: auto;
            min-height: 100%;
            min-width: 100%;
        }
    </style> 
</head>
<body>
    <div class="page" align="center" style="background-image: url({{ public_path('/img/carnet_comercializador_ad.png') }}); background-size: 100% 100%; margin: 0 -8px 0 -8px; height: 260px">
        <div style="position: fixed; width: 100%; bottom: 0; height: 347px">
            <div style="float: left; margin: 0 0 0 10px; bottom: 0">
                {!! DNS1D::getBarcodeHTML($carnet_comercializador->persona->carnet_identidad, "C39", 1, 33) !!}
            </div>
            <div style="float:right; margin: 0 0 0 2">
                {!! QrCode::margin(0)->size(80)->generate(
                    strtoupper($carnet_comercializador->persona->nombre.' '.$carnet_comercializador->persona->primer_apellido.' '.$carnet_comercializador->persona->segundo_apellido).'|'.
                    $carnet_comercializador->persona->carnet_identidad.' '.$carnet_comercializador->persona->departamento_extension->sigla.'|'.
                    $carnet_comercializador->fecha_emision.'-'.$carnet_comercializador->fecha_conclusion
                ); !!}
            </div>
        </div>
        <table>
            <tr>
                <td colspan="2" style="text-align:center; font-size: 9px">
                    <br>
                    <strong>
                        MINISTERIO DE DESARROLLO RURAL Y TIERRAS <br>
                        VICEMINISTERIO DE COCA Y DESARROLLO INTEGRAL <br>
                        DIRECCIÓN GENERAL DE COCA E INDUSTRIALIZACIÓN <br> <br>
                    </strong>
                </td>                
            </tr>
            <tr>
                <td colspan="2" style="text-align:center;font-size:8pt;font-weight:bold;">
                    <br>
                    CARNET DE COMERCIALIZACIÓN DE LA HOJA DE COCA
                </td>
            </tr>
            <tr style="height: 80px">
                <td style="width: 40%">
                    <img height="85" src="{{ public_path().'/'.$carnet_comercializador->persona->fotografia }}">
                </td>
                <td style="font-size: 12px; height: 60%">
                    <strong>C.I. :</strong> {{ $carnet_comercializador->persona->carnet_identidad.' '.$carnet_comercializador->persona->departamento_extension->sigla }} <br>
                    {{ strtoupper($carnet_comercializador->persona->nombre.' '.$carnet_comercializador->persona->primer_apellido.' '.$carnet_comercializador->persona->segundo_apellido) }} <br>
                    <strong>DESTINO FINAL</strong>  <br>
                    <strong>{{ $carnet_comercializador->persona->tipo_persona->nombre }}</strong>
                </td>
            </tr>
        </table> <br>
    </div>
    <div class="page" align="center" style="background-image: url({{ public_path('/img/carnet_comercializador_at.png') }}); background-size: 100% 100%; margin: 0 -8px 0 -8px; height: 258px">
        <table>
            <tr>
                <td colspan="2" style="text-align:center; font-size: 9px">
                    <br>
                    <strong>
                        MINISTERIO DE DESARROLLO RURAL Y TIERRAS <br>
                        VICEMINISTERIO DE COCA Y DESARROLLO INTEGRAL <br>
                        DIRECCIÓN GENERAL DE COCA E INDUSTRIALIZACIÓN <br> <br>
                    </strong>
                </td>                
            </tr>
            <tr>
                <td colspan="2" style="text-align:center;font-size:7.5pt;font-weight:bold;">
                    LA PRESENTE AUTORIZA A SU TITULAR EL TRANSPORTE Y <br>
                    COMERCIALIZACIÓN DE LA HOJA DE COCA EN EL LUGAR DE SU <br>
                    DESTINO FINAL DE ACUERDO A LA "LEY GENERAL DE LA COCA <br>
                    N° 906" Y DENTRO DEL PLAZO ESTABLECIDO
                </td>
            </tr>
            <tr>
                <td style="font-size: 10px; text-align: left">
                    VÁLIDO {{ $carnet_comercializador->fecha_conclusion }}
                </td>
                <td style="text-align: right; font-size: 10px">
                    DECRETO SUPREMO N°3318
                </td>
            </tr>
            <tr>
                <td style="height: 105px; bottom: 0; text-align:center; font-size: 8px; width: 50%">
                    <img src="{{ public_path().'/'.$firma_viceministro->img }}" height="60px"> <br>
                    <strong>{{ $firma_viceministro->nombre }}</strong><br>
                    <strong>DIRECTOR GENERAL</strong> <br>
                    <strong>{{ $firma_viceministro->institucion }}</strong>
                </td>
                <td style="height: 105px; bottom: 0; text-align:center; font-size: 8px; width: 50%">
                    <img src="{{ public_path().'/'.$firma_director->img }}" height="60px"> <br>
                    <strong>{{ $firma_director->nombre }}</strong><br>
                    <strong>DIRECTOR GENERAL</strong> <br>
                    <strong>{{ $firma_director->institucion }}</strong>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: 8px; text-align: center">
                        <strong>EN CASO DE EXTRAVIO SE AGRADECE DEVOLVER A OFICINAS DE DIGCOIN TEL.: 2124263</strong> 
                </td>
            </tr>
        </table>
    </div>
</body>
</html>