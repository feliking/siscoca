<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> 
    <style type="text/css" media="print">
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
    </style> 
</head>
<body>    
        <div class="page" align="center" style="background-image: url({{ public_path('/img/carnet_productor_ad.jpg') }}); background-size: 100% 100%; margin: 0 -8px 0 -8px; height: 260px">
            <div style="position: fixed; width: 100%; bottom: 0; height: 420px">
                <div style="float:right; margin: 0 0 0 2">
                    {!! QrCode::margin(0)->size(80)->generate(
                        strtoupper($carnet_productor->persona->nombre.' '.$carnet_productor->persona->primer_apellido.' '.$carnet_productor->persona->segundo_apellido).'|'.
                        $carnet_productor->persona->carnet_identidad.' '.$carnet_productor->persona->departamento_extension->sigla.'|'.
                        $carnet_productor->fecha_emision.'-'.$carnet_productor->fecha_conclusion
                    ); !!}
                </div>
            </div>
            <table>
                <tr>
                    <td style="height: 50px"></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;font-size:9pt;font-weight:bold;">
                        <br>
                        CARNET DE PRODUCTOR DE COCA <br>
                        <span style="font-size: 8pt"><strong>DEPARTAMENTO DE LA PAZ</strong></span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%">
                        <img style="object-fit: cover" height="85" width="130" src="{{ public_path().'/'.$carnet_productor->persona->fotografia }}">
                    </td>
                    <td style="width: 80%; font-size: 10px">
                        <strong>PROVINCIA. :</strong> {{ $carnet_productor->provincia->nombre }} <br>
                        <strong>MUNICIPIO :</strong> {{ $carnet_productor->municipio->nombre }} <br>
                        <strong>FEDERACION :</strong> {{ $carnet_productor->federacion->nombre }} <br>
                        <strong>CENTRAL :</strong> {{ $carnet_productor->central->nombre }} <br>
                        <strong>NOMBRE :</strong> {{ $carnet_productor->persona->nombre.' '.$carnet_productor->persona->primer_apellido.' '.$carnet_productor->persona->segundo_apellido }} <br>
                        <strong>C.I. :</strong> {{ $carnet_productor->persona->carnet_identidad.' '.$carnet_productor->persona->departamento_extension->sigla }} <br>
                    </td>
                </tr>
            </table> <br>
        </div>
        <div class="page" align="center" style="background-image: url({{ public_path('/img/carnet_productor_at.jpg') }}); background-size: 100% 100%; margin: 0 -8px 0 -8px; height: 258px">

        </div>
</body>
</html>