<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Deben tener al menos 6 caracteres y coincidir con la confirmación.',
    'reset' => 'Tu contraseña fue reseteada!',
    'sent' => 'Le enviamos un correo para recuperera su contraseña!',
    'token' => 'Token invalido.',
    'user' => "No podemos encontrar un usuario con este email.",

];
