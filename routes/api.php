<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['guest:api']], function() {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('login/refresh', 'Auth\LoginController@refresh');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('persona/quest', 'PersonaController@quest');
    // portal informativo
    Route::resource('portal_normativa', 'NormativaController');   
    Route::resource('portal_proy_obra', 'ProyObraProductorController');                
    Route::resource('portal_prod_autorizado', 'ProductoAutorizadoController');
    Route::resource('portal_prod_industrializado', 'ProductoIndustrializadoController');   
    Route::resource('portal_investigacion_coca', 'InvestigacionCocaController');  
    Route::resource('portal_enlace_institucional', 'EnlaceInstitucionalController');
    Route::resource('portal_reclamo', 'ReclamoController');          
    Route::resource('portal_consulta', 'ConsultaController');          
    Route::resource('portal_callcenter', 'CallCenterController');           
});
Route::group(['middleware' => ['jwt']], function() {
    Route::post('logout', 'Auth\LoginController@logout');
    Route::get('me', 'Auth\LoginController@me');
    Route::put('profile', 'ProfileController@update');
    Route::put('updatepass/{id}', 'UserController@updatepass');
    //ROLES 1
    Route::group(['middleware' => ['role:admin']], function () {
        Route::resource('user', 'UserController');
        Route::resource('role', 'RoleController');
        Route::get('role/fill/{param}', 'RoleController@fill');
        Route::resource('permission', 'PermissionController');

        //Auditoria
        Route::get('action/index', 'ActionController@index')->middleware('permission:listar'); 
        
        Route::get('evento/fill/{param}', 'EventoController@fill')->middleware('permission:listar');
        Route::get('evento/index', 'EventoController@index')->middleware('permission:listar');
        Route::post('evento', 'EventoController@store')->middleware('permission:registrar');
        Route::put('evento/{id}', 'EventoController@update')->middleware('permission:editar');
        Route::delete('evento/{id}', 'EventoController@destroy')->middleware('permission:eliminar');
        Route::get('tipo_prioridad', 'TipoPrioridadController@index')->middleware('permission:listar');
        Route::get('firmas', 'FirmaController@index')->middleware('permission:listar');
        Route::post('firmas', 'FirmaController@store')->middleware('permission:registrar');
        Route::put('firmas/{id}', 'FirmaController@update')->middleware('permission:registrar');
        Route::delete('firmas', 'FirmaController@destroy')->middleware('permission:registrar');
        //REPORTES
        Route::get('reporte/excel', 'ReporteExcelController@reporte');
        Route::get('reporte/pdf', 'ReportePDFController@reporte');        
        //TODO: poner en un nuevo option
    });
    Route::group(['middleware' => ['role:admin|digprococa']], function () {
        Route::get('carnet_productor/print/{id}', 'CarnetProductorController@print')->middleware('permission:imprimir');
        Route::get('carnet_productor/fill/{param}', 'CarnetProductorController@fill')->middleware('permission:listar');
        Route::get('carnet_productor', 'CarnetProductorController@index')->middleware('permission:listar');
        Route::get('carnet_productor/{id}', 'CarnetProductorController@show')->middleware('permission:listar');
        Route::post('carnet_productor', 'CarnetProductorController@store')->middleware('permission:registrar');
        Route::put('carnet_productor/{id}', 'CarnetProductorController@update')->middleware('permission:editar');
        Route::delete('carnet_productor/{id}', 'CarnetProductorController@destroy')->middleware('permission:eliminar');
        Route::get('zonas_racionalizadas', 'zonasRacionalizadasController@index')->middleware('permission:listar');
        Route::post('zonas_racionalizadas','zonasRacionalizadasController@store')->middleware('permission:registrar');
        Route::get('parcela', 'ParcelaController@index')->middleware('permission:listar');
        Route::get('parcela/fullsearch', 'ParcelaController@fullsearch')->middleware('permission:listar');
        Route::get('parcela/productor/{id}', 'ParcelaController@getParcelaByProductor')->middleware('permission:listar');
        Route::get('ejecucion/','tipoEjecucionController@index')->middleware('permission:listar');
    });
    Route::group(['middleware' => ['role:admin|digcoin']], function () {
        Route::get('empresa/fill/{param}', 'EmpresaController@fill')->middleware('permission:listar');
        Route::get('empresa', 'EmpresaController@index')->middleware('permission:listar');
        Route::post('empresa', 'EmpresaController@store')->middleware('permission:registrar');
        Route::put('empresa/{id}', 'EmpresaController@update')->middleware('permission:editar');
        Route::delete('empresa/{id}', 'EmpresaController@destroy')->middleware('permission:eliminar');

        Route::get('autorizacion/print/{id}', 'AutorizacionController@print')->middleware('permission:imprimir');
        Route::get('autorizacion/fill/{param}', 'AutorizacionController@fill')->middleware('permission:listar');
        Route::get('autorizacion', 'AutorizacionController@index')->middleware('permission:listar');
        Route::get('autorizacion/{id}', 'AutorizacionController@show')->middleware('permission:listar');
        Route::post('autorizacion', 'AutorizacionController@store')->middleware('permission:registrar');
        Route::put('autorizacion/{id}', 'AutorizacionController@update')->middleware('permission:editar');
        Route::delete('autorizacion/{id}', 'AutorizacionController@destroy')->middleware('permission:eliminar');

        Route::get('entidad_investigacion/fill/{param}', 'EntidadInvestigacionController@fill')->middleware('permission:listar');
        Route::get('entidad_investigacion', 'EntidadInvestigacionController@index')->middleware('permission:listar');
        Route::get('entidad_investigacion/{id}', 'EntidadInvestigacionController@show')->middleware('permission:listar');
        Route::post('entidad_investigacion', 'EntidadInvestigacionController@store')->middleware('permission:registrar');
        Route::put('entidad_investigacion/{id}', 'EntidadInvestigacionController@update')->middleware('permission:editar');
        Route::delete('entidad_investigacion/{id}', 'EntidadInvestigacionController@destroy')->middleware('permission:eliminar');

        Route::get('beneficiario_donacion/fill/{param}', 'BeneficiarioDonacionController@fill')->middleware('permission:listar');
        Route::get('beneficiario_donacion', 'BeneficiarioDonacionController@index')->middleware('permission:listar');
        Route::get('beneficiario_donacion/{id}', 'BeneficiarioDonacionController@show')->middleware('permission:listar');
        Route::post('beneficiario_donacion', 'BeneficiarioDonacionController@store')->middleware('permission:registrar');
        Route::put('beneficiario_donacion/{id}', 'BeneficiarioDonacionController@update')->middleware('permission:editar');
        Route::delete('beneficiario_donacion/{id}', 'BeneficiarioDonacionController@destroy')->middleware('permission:eliminar');

        Route::get('guia_internacion/print/{id}', 'GuiaInternacionController@print')->middleware('permission:imprimir');
        Route::get('guia_internacion/max/{param}', 'GuiaInternacionController@max')->middleware('permission:listar');
        Route::get('guia_internacion/fill/{param}', 'GuiaInternacionController@fill')->middleware('permission:listar');
        Route::get('guia_internacion', 'GuiaInternacionController@index')->middleware('permission:listar');
        Route::post('guia_internacion', 'GuiaInternacionController@store')->middleware('permission:registrar');
        Route::put('guia_internacion/{id}', 'GuiaInternacionController@update')->middleware('permission:editar');
        Route::delete('guia_internacion/{id}', 'GuiaInternacionController@destroy')->middleware('permission:eliminar');

        Route::get('control_guia_internacion/fill/{param}', 'ControlGuiaInternacionController@fill')->middleware('permission:listar');
        Route::get('control_guia_internacion', 'ControlGuiaInternacionController@index')->middleware('permission:listar');
        Route::post('control_guia_internacion', 'ControlGuiaInternacionController@store')->middleware('permission:registrar');
        Route::put('control_guia_internacion/{id}', 'ControlGuiaInternacionController@update')->middleware('permission:editar');
        Route::delete('control_guia_internacion/{id}', 'ControlGuiaInternacionController@destroy')->middleware('permission:eliminar');

        Route::get('hoja_ruta/max/{param}', 'HojaRutaController@max')->middleware('permission:listar');
        Route::get('hoja_ruta/fill/{param}', 'HojaRutaController@fill')->middleware('permission:listar');
        Route::get('hoja_ruta/print/{id}', 'HojaRutaController@print')->middleware('permission:imprimir');
        Route::get('hoja_ruta', 'HojaRutaController@index')->middleware('permission:listar');
        Route::post('hoja_ruta', 'HojaRutaController@store')->middleware('permission:registrar');
        Route::put('hoja_ruta/{id}', 'HojaRutaController@update')->middleware('permission:editar');
        Route::delete('hoja_ruta/{id}', 'HojaRutaController@destroy')->middleware('permission:eliminar');
        Route::get('hoja_ruta/maxTrueque/{param}', 'HojaRutaController@maxTrueque')->middleware('permission:listar');



        Route::get('control_hoja_ruta/fill/{params}', 'ControlHojaRutaController@fill')->middleware('permission:listar');
        Route::get('control_hoja_ruta', 'ControlHojaRutaController@index')->middleware('permission:listar');
        Route::post('control_hoja_ruta', 'ControlHojaRutaController@store')->middleware('permission:registrar');
        Route::put('control_hoja_ruta/{id}', 'ControlHojaRutaController@update')->middleware('permission:editar');
        Route::delete('control_hoja_ruta/{id}', 'ControlHojaRutaController@destroy')->middleware('permission:eliminar');

        Route::get('autorizacion_cpv/fill/{params}', 'AutorizacionCambioPuestoVentaController@fill')->middleware('permission:listar');
        Route::get('autorizacion_cpv', 'AutorizacionCambioPuestoVentaController@index')->middleware('permission:listar');
        Route::post('autorizacion_cpv', 'AutorizacionCambioPuestoVentaController@store')->middleware('permission:registrar');
        Route::put('autorizacion_cpv/{id}', 'AutorizacionCambioPuestoVentaController@update')->middleware('permission:editar');
        Route::delete('autorizacion_cpv/{id}', 'AutorizacionCambioPuestoVentaController@destroy')->middleware('permission:eliminar');

        Route::get('poder/fill/{params}', 'PoderController@fill')->middleware('permission:listar');
        Route::get('poder', 'PoderController@index')->middleware('permission:listar');
        Route::post('poder', 'PoderController@store')->middleware('permission:registrar');
        Route::put('poder/{id}', 'PoderController@update')->middleware('permission:editar');
        Route::delete('poder/{id}', 'PoderController@destroy')->middleware('permission:eliminar');

        Route::get('puesto_venta/fill/{params}', 'PuestoVentaController@fill')->middleware('permission:listar');
        Route::get('puesto_venta', 'PuestoVentaController@index')->middleware('permission:listar');
        Route::post('puesto_venta', 'PuestoVentaController@store')->middleware('permission:registrar');
        Route::put('puesto_venta/{id}', 'PuestoVentaController@update')->middleware('permission:editar');
        Route::delete('puesto_venta/{id}', 'PuestoVentaController@destroy')->middleware('permission:eliminar');

        Route::get('persona_puesto_venta/fill/{params}', 'PersonaPuestoVentaController@fill')->middleware('permission:listar');
        Route::get('persona_puesto_venta', 'PersonaPuestoVentaController@index')->middleware('permission:listar');
        Route::post('persona_puesto_venta', 'PersonaPuestoVentaController@store')->middleware('permission:registrar');
        Route::put('persona_puesto_venta/{id}', 'PersonaPuestoVentaController@update')->middleware('permission:editar');
        Route::delete('persona_puesto_venta/{id}', 'PersonaPuestoVentaController@destroy')->middleware('permission:eliminar');

        Route::get('retencion/print/{id}', 'RetencionController@print')->middleware('permission:imprimir');
        Route::get('retencion/printhoja/{id}', 'RetencionController@printHoja')->middleware('permission:imprimir');
        Route::get('retencion/max/{param}', 'RetencionController@max')->middleware('permission:listar');
        Route::get('retencion/fill/{param}', 'RetencionController@fill')->middleware('permission:listar');
        Route::get('retencion', 'RetencionController@index')->middleware('permission:listar');
        Route::post('retencion', 'RetencionController@store')->middleware('permission:registrar');
        Route::put('retencion/{id}', 'RetencionController@update')->middleware('permission:editar');
        Route::delete('retencion/{id}', 'RetencionController@destroy')->middleware('permission:eliminar');

        Route::get('deposito/saldo', 'DepositoController@saldo')->middleware('permission:listar');
        Route::get('deposito/max/{param}', 'DepositoController@max')->middleware('permission:listar');
        Route::get('deposito/fill/{params}', 'DepositoController@fill')->middleware('permission:listar');
        Route::get('deposito', 'DepositoController@index')->middleware('permission:listar');
        Route::post('deposito', 'DepositoController@store')->middleware('permission:registrar');
        Route::put('deposito/{id}', 'DepositoController@update')->middleware('permission:editar');
        Route::delete('deposito/{id}', 'DepositoController@destroy')->middleware('permission:eliminar');

        Route::get('decomiso/fill/{param}', 'DecomisoController@fill')->middleware('permission:listar');
        Route::get('decomiso', 'DecomisoController@index')->middleware('permission:listar');
        Route::post('decomiso', 'DecomisoController@store')->middleware('permission:registrar');
        Route::put('decomiso/{id}', 'DecomisoController@update')->middleware('permission:editar');
        Route::delete('decomiso/{id}', 'DecomisoController@destroy')->middleware('permission:eliminar');

        Route::get('control_puesto/fill/{params}', 'ControlPuestoController@fill')->middleware('permission:listar');
        Route::get('control_puesto', 'ControlPuestoController@index')->middleware('permission:listar');
        Route::post('{hoja_ruta_id}/control_puesto', 'ControlPuestoController@store')->middleware('permission:registrar');
        Route::put('control_puesto/{id}', 'ControlPuestoController@update')->middleware('permission:editar');
        Route::delete('control_puesto/{id}', 'ControlPuestoController@destroy')->middleware('permission:eliminar');

        Route::get('donacion/fill/{params}', 'DonacionController@fill')->middleware('permission:listar');
        Route::get('donacion', 'DonacionController@index')->middleware('permission:listar');
        Route::post('donacion', 'DonacionController@store')->middleware('permission:registrar');
        Route::put('donacion/{id}', 'DonacionController@update')->middleware('permission:editar');
        Route::delete('donacion/{id}', 'DonacionController@destroy')->middleware('permission:eliminar');

        Route::get('incineracion/fill/{params}', 'IncineracionController@fill')->middleware('permission:listar');
        Route::get('incineracion', 'IncineracionController@index')->middleware('permission:listar');
        Route::post('incineracion', 'IncineracionController@store')->middleware('permission:registrar');
        Route::put('incineracion/{id}', 'IncineracionController@update')->middleware('permission:editar');
        Route::delete('incineracion/{id}', 'IncineracionController@destroy')->middleware('permission:eliminar');

        Route::get('devolucion/fill/{params}', 'DevolucionController@fill')->middleware('permission:listar');
        Route::get('devolucion', 'DevolucionController@index')->middleware('permission:listar');
        Route::post('devolucion', 'DevolucionController@store')->middleware('permission:registrar');
        Route::put('devolucion/{id}', 'DevolucionController@update')->middleware('permission:editar');
        Route::delete('devolucion/{id}', 'DevolucionController@destroy')->middleware('permission:eliminar');
    });
    Route::group(['middleware' => ['role:admin|digprococa|digcoin|oii']], function () {
        Route::get('persona/fill/{param}', 'PersonaController@fill')->middleware('permission:listar');
        Route::get('persona/hoja_ruta', 'PersonaController@hoja_ruta');
        Route::get('persona', 'PersonaController@index')->middleware('permission:listar');
        Route::get('productor', 'PersonaController@productorSearch')->middleware('permission:listar');
        Route::post('persona', 'PersonaController@store')->middleware('permission:registrar');
        Route::put('persona/{id}', 'PersonaController@update')->middleware('permission:editar');
        Route::delete('persona/{id}', 'PersonaController@destroy')->middleware('permission:eliminar');

        Route::get('tipo_persona', 'TipoPersonaController@index')->middleware('permission:listar');
        Route::post('tipo_persona', 'TipoPersonaController@store')->middleware('permission:registrar');
        Route::put('tipo_persona/{id}', 'TipoPersonaController@update')->middleware('permission:editar');
        Route::delete('tipo_persona/{id}', 'TipoPersonaController@destroy')->middleware('permission:eliminar');

        Route::get('cesacion/fill/{param}', 'CesacionController@fill')->middleware('permission:listar');
        Route::get('cesacion', 'CesacionController@index')->middleware('permission:listar');
        Route::post('cesacion', 'CesacionController@store')->middleware('permission:registrar');
        Route::put('cesacion/{id}', 'CesacionController@update')->middleware('permission:editar');
        Route::delete('cesacion/{id}', 'CesacionController@destroy')->middleware('permission:eliminar');

        Route::get('persona_cesacion/fill/{param}', 'PersonaCesacionController@fill')->middleware('permission:listar');
        Route::get('persona_cesacion', 'PersonaCesacionController@index')->middleware('permission:listar');
        Route::post('persona_cesacion', 'PersonaCesacionController@store')->middleware('permission:registrar');
        Route::put('persona_cesacion/{id}', 'PersonaCesacionController@update')->middleware('permission:editar');
        Route::delete('persona_cesacion/{id}', 'PersonaCesacionController@destroy')->middleware('permission:eliminar');

        Route::get('sustitucion/fill/{param}', 'SustitucionController@fill')->middleware('permission:listar');
        Route::get('sustitucion', 'SustitucionController@index')->middleware('permission:listar');
        Route::post('sustitucion', 'SustitucionController@store')->middleware('permission:registrar');
        Route::put('sustitucion/{id}', 'SustitucionController@update')->middleware('permission:editar');
        Route::delete('sustitucion/{id}', 'SustitucionController@destroy')->middleware('permission:eliminar');

        Route::get('comunidad/fill/{param}', 'ComunidadController@fill')->middleware('permission:listar');
        Route::get('comunidad', 'ComunidadController@index')->middleware('permission:listar');
        Route::post('comunidad', 'ComunidadController@store')->middleware('permission:registrar');
        Route::put('comunidad/{id}', 'ComunidadController@update')->middleware('permission:editar');
        Route::delete('comunidad/{id}', 'ComunidadController@destroy')->middleware('permission:eliminar');

        Route::get('carnet_comercializador/print/{id}', 'CarnetComercializadorController@print')->middleware('permission:imprimir');
        Route::get('carnet_comercializador/fill/{param}', 'CarnetComercializadorController@fill')->middleware('permission:listar');
        Route::get('carnet_comercializador', 'CarnetComercializadorController@index')->middleware('permission:listar');
        Route::get('carnet_comercializador/{id}', 'CarnetComercializadorController@show')->middleware('permission:listar');
        Route::post('carnet_comercializador', 'CarnetComercializadorController@store')->middleware('permission:registrar');
        Route::put('carnet_comercializador/{id}', 'CarnetComercializadorController@update')->middleware('permission:editar');
        Route::delete('carnet_comercializador/{id}', 'CarnetComercializadorController@destroy')->middleware('permission:eliminar');

        Route::get('resolucion_administrativa/fill/{param}', 'ResolucionAdministrativaController@fill');
        Route::get('resolucion_administrativa', 'ResolucionAdministrativaController@index')->middleware('permission:listar');
        Route::post('resolucion_administrativa', 'ResolucionAdministrativaController@store')->middleware('permission:registrar');
        Route::put('resolucion_administrativa/{id}', 'ResolucionAdministrativaController@update')->middleware('permission:editar');
        Route::delete('resolucion_administrativa/{id}', 'ResolucionAdministrativaController@destroy')->middleware('permission:eliminar');
        
        Route::get('tipo_resolucion', 'TipoResolucionController@index')->middleware('permission:listar');
        Route::post('tipo_resolucion', 'TipoResolucionController@store')->middleware('permission:registrar');
        Route::put('tipo_resolucion/{id}', 'TipoResolucionController@update')->middleware('permission:editar');
        Route::delete('tipo_resolucion/{id}', 'TipoResolucionController@destroy')->middleware('permission:eliminar');

        Route::get('tipo_sancion', 'TipoSancionController@index')->middleware('permission:listar');
        Route::post('tipo_sancion', 'TipoSancionController@store')->middleware('permission:registrar');
        Route::put('tipo_sancion/{id}', 'TipoSancionController@update')->middleware('permission:editar');
        Route::delete('tipo_sancion/{id}', 'TipoSancionController@destroy')->middleware('permission:eliminar');

        Route::get('sancion/fill/{param}', 'SancionController@fill');
        Route::get('sancion/showfill/{param}', 'SancionController@showfill');
        Route::get('sancion', 'SancionController@index')->middleware('permission:listar');
        Route::post('sancion', 'SancionController@store')->middleware('permission:registrar');
        Route::put('sancion/{id}', 'SancionController@update')->middleware('permission:editar');
        Route::delete('sancion/{id}', 'SancionController@destroy')->middleware('permission:eliminar');
        
        Route::get('infraccion/fill/{param}', 'InfraccionController@fill');
        Route::get('infraccion', 'InfraccionController@index')->middleware('permission:listar');
        Route::post('infraccion', 'InfraccionController@store')->middleware('permission:registrar');
        Route::put('infraccion/{id}', 'InfraccionController@update')->middleware('permission:editar');
        Route::delete('infraccion/{id}', 'InfraccionController@destroy')->middleware('permission:eliminar');

        Route::get('comercializador_infraccion/fill/{param}', 'ComercializadorInfraccionController@fill');
        Route::get('comercializador_infraccion', 'ComercializadorInfraccionController@index')->middleware('permission:listar');
        Route::post('comercializador_infraccion', 'ComercializadorInfraccionController@store')->middleware('permission:registrar');
        Route::put('comercializador_infraccion/{id}', 'ComercializadorInfraccionController@update')->middleware('permission:editar');
        Route::delete('comercializador_infraccion/{sancion_id}', 'ComercializadorInfraccionController@destroy')->middleware('permission:eliminar');

        Route::get('motivoactualizacion/fill/{params}', 'MotivoActualizacionController@fill')->middleware('permission:listar');
        Route::get('motivoactualizacion', 'MotivoActualizacionController@index')->middleware('permission:listar');
        Route::post('motivoactualizacion', 'MotivoActualizacionController@store')->middleware('permission:registrar');
        Route::put('motivoactualizacion/{id}', 'MotivoActualizacionController@update')->middleware('permission:editar');
        Route::delete('motivoactualizacion/{id}', 'MotivoActualizacionController@destroy')->middleware('permission:eliminar');
        
        Route::get('parcela/fill/{param}', 'ParcelaController@fill');
        Route::get('parcela', 'ParcelaController@index')->middleware('permission:listar');  
        Route::post('parcela', 'ParcelaController@store')->middleware('permission:registrar');
        Route::put('parcela/{id}', 'ParcelaController@update')->middleware('permission:editar');
        Route::delete('parcela/{id}', 'ParcelaController@destroy')->middleware('permission:eliminar');
        Route::get('parcela/showfill/{param}', 'ParcelaController@showfill');
        Route::put('parcela/edit/{id}', 'ParcelaController@edit')->middleware('permission:editar');
        Route::get('parcela/showfillver/{param}', 'ParcelaController@showfillver');

        Route::get('autorizacionrenovacion/fill/{param}', 'AutorizacionRenovacionController@fill');
        Route::get('autorizacionrenovacion/showfill/{id}', 'AutorizacionRenovacionController@showfill');
        Route::get('autorizacionrenovacion', 'AutorizacionRenovacionController@index')->middleware('permission:listar');
        Route::post('autorizacionrenovacion', 'AutorizacionRenovacionController@store')->middleware('permission:registrar');
        Route::put('autorizacionrenovacion/{id}', 'AutorizacionRenovacionController@update')->middleware('permission:editar');
        Route::delete('autorizacionrenovacion/{id}', 'AutorizacionRenovacionController@destroy')->middleware('permission:eliminar');

        // Módulo 5: Desarrollo integral
        Route::get('proyecto/fill/{param}', 'DesarrolloIntegral\ProyectoController@fill')->middleware('permission:listar');
        Route::get('proyecto', 'DesarrolloIntegral\ProyectoController@index')->middleware('permission:listar');
        Route::post('proyecto', 'DesarrolloIntegral\ProyectoController@store')->middleware('permission:registrar');
        Route::put('proyecto/{id}', 'DesarrolloIntegral\ProyectoController@update')->middleware('permission:editar');
        Route::delete('proyecto/{id}', 'DesarrolloIntegral\ProyectoController@destroy')->middleware('permission:eliminar');

        Route::get('proyecto/fill/{param}', 'DesarrolloIntegral\ProyectoController@fill')->middleware('permission:listar');
        Route::get('proyecto', 'DesarrolloIntegral\ProyectoController@index')->middleware('permission:listar');
        Route::post('proyecto', 'DesarrolloIntegral\ProyectoController@store')->middleware('permission:registrar');
        Route::put('proyecto/{id}', 'DesarrolloIntegral\ProyectoController@update')->middleware('permission:editar');
        Route::delete('proyecto/{id}', 'DesarrolloIntegral\ProyectoController@destroy')->middleware('permission:eliminar');   

        Route::get('tipo_convocatoria/fill/{param}', 'DesarrolloIntegral\TipoConvocatoriaController@fill')->middleware('permission:listar');
        Route::get('tipo_convocatoria', 'DesarrolloIntegral\TipoConvocatoriaController@index')->middleware('permission:listar');
        Route::post('tipo_convocatoria', 'DesarrolloIntegral\TipoConvocatoriaController@store')->middleware('permission:registrar');
        Route::put('tipo_convocatoria/{id}', 'DesarrolloIntegral\TipoConvocatoriaController@update')->middleware('permission:editar');
        Route::delete('tipo_convocatoria/{id}', 'DesarrolloIntegral\TipoConvocatoriaController@destroy')->middleware('permission:eliminar');

        Route::get('convocatoria/fill/{param}', 'DesarrolloIntegral\ConvocatoriaController@fill')->middleware('permission:listar');
        Route::get('convocatoria', 'DesarrolloIntegral\ConvocatoriaController@index')->middleware('permission:listar');
        Route::post('convocatoria', 'DesarrolloIntegral\ConvocatoriaController@store')->middleware('permission:registrar');
        Route::put('convocatoria/{id}', 'DesarrolloIntegral\ConvocatoriaController@update')->middleware('permission:editar');
        Route::delete('convocatoria/{id}', 'DesarrolloIntegral\ConvocatoriaController@destroy')->middleware('permission:eliminar');
       
        Route::get('proyecto/priorizacionfill', 'DesarrolloIntegral\ProyectoController@priorizacionfill')->middleware('permission:listar');
        Route::get('proyecto/showfill', 'DesarrolloIntegral\ProyectoController@showfill')->middleware('permission:listar');
        Route::get('proyecto/supervisionfill', 'DesarrolloIntegral\ProyectoController@supervisionfill')->middleware('permission:listar');
        Route::get('proyecto/cierreobrafill', 'DesarrolloIntegral\ProyectoController@cierreobrafill')->middleware('permission:listar');
        Route::get('proyecto/evaluacionfill', 'DesarrolloIntegral\ProyectoController@evaluacionfill')->middleware('permission:listar');


        Route::get('solicitud/fill/{param}', 'DesarrolloIntegral\SolicitudController@fill')->middleware('permission:listar');
        Route::get('solicitud', 'DesarrolloIntegral\SolicitudController@index')->middleware('permission:listar');
        Route::post('solicitud', 'DesarrolloIntegral\SolicitudController@store')->middleware('permission:registrar');
        Route::put('solicitud/{id}', 'DesarrolloIntegral\SolicitudController@update')->middleware('permission:editar');
        Route::delete('solicitud/{id}', 'DesarrolloIntegral\SolicitudController@destroy')->middleware('permission:eliminar');
        Route::get('solicitud/print/{id}', 'DesarrolloIntegral\SolicitudController@print')->middleware('permission:imprimir');

        Route::get('priorizacion/fill/{param}', 'DesarrolloIntegral\PriorizacionController@fill')->middleware('permission:listar');
        Route::get('priorizacion', 'DesarrolloIntegral\PriorizacionController@index')->middleware('permission:listar');
        Route::post('priorizacion', 'DesarrolloIntegral\PriorizacionController@store')->middleware('permission:registrar');
        Route::put('priorizacion/{id}', 'DesarrolloIntegral\PriorizacionController@update')->middleware('permission:editar');
        Route::delete('priorizacion/{id}', 'DesarrolloIntegral\PriorizacionController@destroy')->middleware('permission:eliminar');
        Route::get('priorizacion/print/{id}', 'DesarrolloIntegral\PriorizacionController@print')->middleware('permission:imprimir');

        Route::get('evaluacionviabilidad/fill/{param}', 'DesarrolloIntegral\EvaluacionViabilidadController@fill')->middleware('permission:listar');
        Route::get('evaluacionviabilidad', 'DesarrolloIntegral\EvaluacionViabilidadController@index')->middleware('permission:listar');
        Route::post('evaluacionviabilidad', 'DesarrolloIntegral\EvaluacionViabilidadController@store')->middleware('permission:registrar');
        Route::put('evaluacionviabilidad/{id}', 'DesarrolloIntegral\EvaluacionViabilidadController@update')->middleware('permission:editar');
        Route::delete('evaluacionviabilidad/{id}', 'DesarrolloIntegral\EvaluacionViabilidadController@destroy')->middleware('permission:eliminar');
        Route::get('evaluacionviabilidad/print/{id}', 'DesarrolloIntegral\EvaluacionViabilidadController@print')->middleware('permission:imprimir');

        Route::get('supervision/fill/{param}', 'DesarrolloIntegral\SupervisionController@fill')->middleware('permission:listar');
        Route::get('supervision', 'DesarrolloIntegral\SupervisionController@index')->middleware('permission:listar');
        Route::post('supervision', 'DesarrolloIntegral\SupervisionController@store')->middleware('permission:registrar');
        Route::put('supervision/{id}', 'DesarrolloIntegral\SupervisionController@update')->middleware('permission:editar');
        Route::delete('supervision/{id}', 'DesarrolloIntegral\SupervisionController@destroy')->middleware('permission:eliminar');

        Route::get('cierreobra/fill/{param}', 'DesarrolloIntegral\CierreObraController@fill')->middleware('permission:listar');
        Route::get('cierreobra', 'DesarrolloIntegral\CierreObraController@index')->middleware('permission:listar');
        Route::post('cierreobra', 'DesarrolloIntegral\CierreObraController@store')->middleware('permission:registrar');
        Route::put('cierreobra/{id}', 'DesarrolloIntegral\CierreObraController@update')->middleware('permission:editar');
        Route::delete('cierreobra/{id}', 'DesarrolloIntegral\CierreObraController@destroy')->middleware('permission:eliminar');

        Route::get('evaluacionimpacto/fill/{param}', 'DesarrolloIntegral\EvaluacionImpactoController@fill')->middleware('permission:listar');
        Route::get('evaluacionimpacto', 'DesarrolloIntegral\EvaluacionImpactoController@index')->middleware('permission:listar');
        Route::post('evaluacionimpacto', 'DesarrolloIntegral\EvaluacionImpactoController@store')->middleware('permission:registrar');
        Route::put('evaluacionimpacto/{id}', 'DesarrolloIntegral\EvaluacionImpactoController@update')->middleware('permission:editar');
        Route::delete('evaluacionimpacto/{id}', 'DesarrolloIntegral\EvaluacionImpactoController@destroy')->middleware('permission:eliminar');

        Route::get('evaluacionimpacto_oii/fill/{param}', 'DesarrolloIntegral\EvaluacionImpactoController@fill')->middleware('permission:listar');
        Route::get('evaluacionimpacto_oii', 'DesarrolloIntegral\EvaluacionImpactoController@index')->middleware('permission:listar');
        Route::post('evaluacionimpacto_oii', 'DesarrolloIntegral\EvaluacionImpactoController@store')->middleware('permission:registrar');
        Route::put('evaluacionimpacto_oii/{id}', 'DesarrolloIntegral\EvaluacionImpactoController@update')->middleware('permission:editar');
        Route::delete('evaluacionimpacto_oii/{id}', 'DesarrolloIntegral\EvaluacionImpactoController@destroy')->middleware('permission:eliminar');

        Route::get('solicitud/aprobada' , 'DesarrolloIntegral\SolicitudesAprobadasController@index')->middleware('permission:listar');
        Route::get('proyectos/search', 'DesarrolloIntegral\ProyectoController@search')->middleware('permission:listar');
        Route::post('proyectos/{id}/emision_orden', 'DesarrolloIntegral\SolicitudesAprobadasController@saveOrden')->middleware('permission:registrar');
        Route::post('proyectos/{id}/seguimiento', 'DesarrolloIntegral\SolicitudesAprobadasController@saveSeguimiento')->middleware('permission:registrar');
        Route::post('proyectos/{id}/reprogramacion', 'DesarrolloIntegral\SolicitudesAprobadasController@saveReprogramacion')->middleware('permission:registrar');
        Route::post('proyectos/{id}/cancelacion', 'DesarrolloIntegral\SolicitudesAprobadasController@saveCancelacion')->middleware('permission:registrar');
        Route::post('proyectos/{id}/recepcion', 'DesarrolloIntegral\SolicitudesAprobadasController@saveRecepcion')->middleware('permission:registrar');

        Route::get('requisito/fill/{param}', 'DesarrolloIntegral\RequisitoController@fill')->middleware('permission:listar');
        Route::get('requisito', 'DesarrolloIntegral\RequisitoController@index')->middleware('permission:listar');
        Route::post('requisito', 'DesarrolloIntegral\RequisitoController@store')->middleware('permission:registrar');
        Route::put('requisito/{id}', 'DesarrolloIntegral\RequisitoController@update')->middleware('permission:editar');
        Route::delete('requisito/{id}', 'DesarrolloIntegral\RequisitoController@destroy')->middleware('permission:eliminar');

        Route::get('propuesta/fill/{param}', 'DesarrolloIntegral\PropuestaController@fill')->middleware('permission:listar');
        Route::get('propuesta', 'DesarrolloIntegral\PropuestaController@index')->middleware('permission:listar');
        Route::post('propuesta', 'DesarrolloIntegral\PropuestaController@store')->middleware('permission:registrar');
        Route::put('propuesta/{id}', 'DesarrolloIntegral\PropuestaController@update')->middleware('permission:editar');
        Route::delete('propuesta/{id}', 'DesarrolloIntegral\PropuestaController@destroy')->middleware('permission:eliminar');

        Route::get('dictamen_apertura_sobres/fill/{param}', 'DesarrolloIntegral\DictamenAperturaSobresController@fill')->middleware('permission:listar');
        Route::get('dictamen_apertura_sobres', 'DesarrolloIntegral\DictamenAperturaSobresController@index')->middleware('permission:listar');
        Route::post('dictamen_apertura_sobres', 'DesarrolloIntegral\DictamenAperturaSobresController@store')->middleware('permission:registrar');
        Route::put('dictamen_apertura_sobres/{id}', 'DesarrolloIntegral\DictamenAperturaSobresController@update')->middleware('permission:editar');
        Route::delete('dictamen_apertura_sobres/{id}', 'DesarrolloIntegral\DictamenAperturaSobresController@destroy')->middleware('permission:eliminar');

        Route::get('solicitud_aprobada/fill/{param}', 'DesarrolloIntegral\SolicitudesAprobadasController@fill')->middleware('permission:listar');
        Route::get('solicitud_aprobada', 'DesarrolloIntegral\SolicitudesAprobadasController@index')->middleware('permission:listar');
        Route::post('solicitud_aprobada', 'DesarrolloIntegral\SolicitudesAprobadasController@store')->middleware('permission:registrar');
        Route::put('solicitud_aprobada/{id}', 'DesarrolloIntegral\SolicitudesAprobadasController@update')->middleware('permission:editar');
        Route::delete('solicitud_aprobada/{id}', 'DesarrolloIntegral\SolicitudesAprobadasController@destroy')->middleware('permission:eliminar');

        Route::get('evaluacion_cofinanciamiento/fill/{param}', 'DesarrolloIntegral\EvaluacionCofinanciamientoController@fill')->middleware('permission:listar');
        Route::get('evaluacion_cofinanciamiento', 'DesarrolloIntegral\EvaluacionCofinanciamientoController@index')->middleware('permission:listar');
        Route::post('evaluacion_cofinanciamiento', 'DesarrolloIntegral\EvaluacionCofinanciamientoController@store')->middleware('permission:registrar');
        Route::put('evaluacion_cofinanciamiento/{id}', 'DesarrolloIntegral\EvaluacionCofinanciamientoController@update')->middleware('permission:editar');
        Route::delete('evaluacion_cofinanciamiento/{id}', 'DesarrolloIntegral\EvaluacionCofinanciamientoController@destroy')->middleware('permission:eliminar');

        Route::get('ejecucion_proyecto/fill/{param}', 'DesarrolloIntegral\EjecucionProyectoController@fill')->middleware('permission:listar');
        Route::get('ejecucion_proyecto', 'DesarrolloIntegral\EjecucionProyectoController@index')->middleware('permission:listar');
        Route::post('ejecucion_proyecto', 'DesarrolloIntegral\EjecucionProyectoController@store')->middleware('permission:registrar');
        Route::put('ejecucion_proyecto/{id}', 'DesarrolloIntegral\EjecucionProyectoController@update')->middleware('permission:editar');
        Route::delete('ejecucion_proyecto/{id}', 'DesarrolloIntegral\EjecucionProyectoController@destroy')->middleware('permission:eliminar');

        

    });
    Route::group(['middleware' => ['role:admin|digprococa|digcoin|fonadin|oii|cliente']], function () {
        Route::get('zona_autorizada/fill/{param}', 'ZonaAutorizadaController@fill');
        Route::resource('zona_autorizada', 'ZonaAutorizadaController');
        Route::get('departamento/fill/{param}', 'DepartamentoController@fill');
        Route::resource('departamento', 'DepartamentoController');
        Route::get('provincia/fill/{param}', 'ProvinciaController@fill');
        Route::resource('provincia', 'ProvinciaController');
        Route::get('municipio/fill/{param}', 'MunicipioController@fill');
        Route::resource('municipio', 'MunicipioController');
        Route::get('federacion/fill/{param}', 'FederacionController@fill');
        Route::get('/federacion/codigo/{code}', 'FederacionController@codevalidate');
        Route::resource('federacion', 'FederacionController');
        Route::get('pais/fill/{param}', 'PaisController@fill');
        Route::resource('pais', 'PaisController');
        Route::get('regional/fill/{param}', 'RegionalController@fill');
        Route::resource('regional', 'RegionalController');
        Route::get('central/fill/{param}', 'CentralController@fill');
        Route::get('/central/codigo/{code}', 'CentralController@codevalidate');
        Route::resource('central', 'CentralController');
        Route::get('/sindicato/codigo/{code}', 'SindicatoController@codevalidate');
        Route::resource('sindicato', 'SindicatoController');
        Route::get('/infraccion/codigo/{code}', 'InfraccionController@codevalidate');
        Route::resource('infraccion', 'InfraccionController');
        Route::get('/sancion/codigo/{code}', 'SancionController@codevalidate');
        Route::resource('sancion', 'SancionController');
        Route::get('/mercado/codigo/{code}', 'MercadoController@codevalidate');
        Route::resource('mercado', 'MercadoController');
        Route::get('/puesto_de_control/codigo/{code}', 'PuestoControlController@codevalidate');
        Route::resource('puesto_de_control', 'PuestoControlController');
        Route::get('/ruta/codigo/{code}', 'RutaController@codevalidate');
        Route::get('/ruta/fill/{param}', 'RutaController@fill');
        Route::resource('ruta', 'RutaController');
        Route::resource('monto', 'MontoController');
        Route::get('vehiculo/fill/{param}', 'VehiculoController@fill');
        Route::resource('vehiculo', 'VehiculoController');
        Route::get('marca/fill/{param}', 'MarcaController@fill');
        Route::resource('marca', 'MarcaController');     
        Route::post('upload/file', 'UploadController@file');
        Route::post('upload/base64_image', 'UploadController@base64_image');
        Route::post('upload/delete', 'UploadController@delete');
        Route::get('causa_retencion/fill/{param}', 'CausaRetencionController@fill');
        Route::resource('causa_retencion', 'CausaRetencionController');
        Route::get('localidad/fill/{param}', 'LocalidadController@fill');
        Route::resource('localidad', 'LocalidadController');
        Route::get('embarcacion/fill/{param}', 'EmbarcacionController@fill');
        Route::resource('embarcacion', 'EmbarcacionController');
        Route::get('responsable/fill/{param}', 'ResponsableController@fill');
        Route::resource('responsable', 'ResponsableController');  
        Route::get('entidad/fill/{param}', 'EntidadController@fill');
        Route::resource('entidad', 'EntidadController');
        Route::get('motivo_incineracion/fill/{param}', 'MotivoIncineracionController@fill');
        Route::resource('motivo_incineracion', 'MotivoIncineracionController'); 
        Route::get('incineracion_adjunto/fill/{param}', 'IncineracionAdjuntoController@fill');
        Route::resource('incineracion_adjunto', 'IncineracionAdjuntoController');
        Route::get('veedor/fill/{param}', 'VeedorController@fill');
        Route::resource('veedor', 'VeedorController');
        Route::get('unidades','unidadesDependientesController@index');
        //modulo capacitaciones
        Route::get('capacitaciones', 'CapacitacionesController@index')->middleware('permission:listar');
        Route::get('capacitaciones/fullsearch', 'CapacitacionesController@fullSearch')->middleware('permission:listar');
        Route::post('capacitaciones','CapacitacionesController@store')->middleware('permission:registrar');
        Route::post('capacitaciones/{id}/concluir', 'conclusionCapacitacionController@store')->middleware('permission:registrar');
        Route::post('capacitaciones/{id}/evaluar', 'evaluacionImpactoController@store')->middleware('permission:registrar');
        
        //modulo notificaciones
        Route::get('notificaciones/eventos', 'NotificacionesController@index')->middleware('permission:listar');
        Route::post('notificaciones/eventos', 'NotificacionesController@store')->middleware('permission:registrar');
        Route::get('notificaciones/registros', 'RegistroEventosController@index')->middleware('permission:listar');
        Route::get('notificaciones/mis_registros', 'RegistroEventosController@getRegistroByUser')->middleware('permission:listar');
        Route::post('notificaciones/registro_eventos', 'RegistroEventosController@store')->middleware('permission:registrar');
        // modulo de portal informativo
        Route::resource('tipo_normativa', 'TipoNormativaController');
        Route::get('normativa/fill/{param}', 'NormativaController@fill');
        Route::resource('normativa', 'NormativaController'); 
        Route::resource('tipo_proy_obra', 'TipoProyObraProductorController');        
        Route::get('proy_obra/fill/{param}', 'ProyObraProductorController@fill');
        Route::resource('proy_obra', 'ProyObraProductorController');                
        Route::get('prod_autorizado/fill/{param}', 'ProductoAutorizadoController@fill');
        Route::resource('prod_autorizado', 'ProductoAutorizadoController');
        Route::get('prod_industrializado/fill/{param}', 'ProductoIndustrializadoController@fill');
        Route::resource('prod_industrializado', 'ProductoIndustrializadoController');   
        Route::get('investigacion_coca/fill/{param}', 'InvestigacionCocaController@fill');
        Route::resource('investigacion_coca', 'InvestigacionCocaController');  
        Route::get('enlace_institucional/fill/{param}', 'EnlaceInstitucionalController@fill');
        Route::resource('enlace_institucional', 'EnlaceInstitucionalController');
        Route::get('reclamo/fill/{param}', 'ReclamoController@fill');
        Route::resource('reclamo', 'ReclamoController');          
        Route::get('consulta/fill/{param}', 'ConsultaController@fill');
        Route::resource('consulta', 'ConsultaController');          
        Route::get('callcenter/fill/{param}', 'CallCenterController@fill');
        Route::resource('callcenter', 'CallCenterController');
        // Módulo de exposición de servicios
        Route::get('entidad_servicio/fill/{param}', 'ApiServices\EntidadServicioController@fill')->middleware('permission:listar');
        Route::get('entidad_servicio', 'ApiServices\EntidadServicioController@index')->middleware('permission:listar');
        Route::post('entidad_servicio', 'ApiServices\EntidadServicioController@store')->middleware('permission:registrar');
        Route::put('entidad_servicio/{id}', 'ApiServices\EntidadServicioController@update')->middleware('permission:editar');
        Route::delete('entidad_servicio/{id}', 'ApiServices\EntidadServicioController@destroy')->middleware('permission:eliminar');
        Route::post('api_services/credentials', 'ApiServices\ApiKeyGenerateController@create');
        Route::get('api_services/credentials', 'ApiServices\ApiKeyGenerateController@getApiKeys')->middleware('permission:listar');
    });
 
}); 